﻿using System;
using TresureHunt.ScreenCode;

namespace TresureHunt
{
	public class LayoutManager
	{
		public LayoutManager ()
		{
		}
		public int getLayout(BaseActivity baseActivity){
			Type type = baseActivity.GetType();
			int i = 0;
			if (TreasureHunt_Globals.inch >= 7) {
				if (TreasureHunt_Globals.inch >= 9) {
					i = TabletLayoutTen (type);
				}
				if(i ==0)
					i = TabletLayout (type);
			} else {
				i = MobileLayout (type);
			}
			return i;
		}

		public int TabletLayoutTen(Type type){
             if (type == typeof(CreelTransferActivity)) {
				return Resource.Layout.CreelTransfer_Tablet_Ten;
			} else if (type == typeof(CreelUserTansferActivity)) {
				return Resource.Layout.CreelUserTransfer_Tablet_Ten;
			} else if (type == typeof(StarterActivity)) {
				return Resource.Layout.StartPage_Tablet_Ten;
			} else
				return 0;
		}

		public int TabletLayout(Type type){
			if (type == typeof(StarterActivity)) {
				return Resource.Layout.StartPage_Tablet;
			} else if (type == typeof(CreelLinkActivity)) {
				return Resource.Layout.CreelLink;
			} else if (type == typeof(CreelTransferActivity)) {
				return Resource.Layout.CreelTransfer_Tablet;
			} else if (type == typeof(TreasureHuntActivity)) {
				return Resource.Layout.MainGame;
			} else if (type == typeof(CreelUserTansferActivity)) {
				return Resource.Layout.CreelUserTransfer_Tablet;
			} else
				return 0;
		}
		public int MobileLayout(Type type){
			if (type == typeof(StarterActivity)) {
				return Resource.Layout.StartPage;
			}
			else if (type == typeof(CreelLinkActivity)) {
				return Resource.Layout.CreelLink;
			}		
			else if (type == typeof(CreelTransferActivity)) {
				return Resource.Layout.CreelTransfer;
			}
			else if (type == typeof(TreasureHuntActivity)) {
				return Resource.Layout.MainGame;
			}
			else if (type == typeof(CreelUserTansferActivity)) {
				return Resource.Layout.CreelUserTransfer;
			}
			else
				return 0;
		}
	}
}

