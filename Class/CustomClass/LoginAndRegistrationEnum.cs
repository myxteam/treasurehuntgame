﻿using System;

namespace TresureHunt
{
	public class LoginAndRegistrationEnum
	{
		public enum Page
		{
			Login,
			Registration
		};
		private LoginAndRegistrationEnum ()
		{
		}
	}
}

