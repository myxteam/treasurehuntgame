﻿using System;
using TreasureGameModels;
using System.Threading.Tasks;
using Android.Content;

namespace TresureHunt
{
	public class CreelAuthenticationHandler
	{
		bool shown = false;
		ApiResult virtualCurrencyTotal = null;
		BaseActivity context;
		LogInModel lmodel;
		public CreelAuthenticationHandler (LogInModel lmodel, BaseActivity context)
		{
			this.lmodel = lmodel;
			this.context = context;
		}

		public async Task<bool> CheckReauthorize ()
		{
			try {
				virtualCurrencyTotal = await ApiCallsExtendtion.Instance.AuthorizationCheckCreel (lmodel, false, context);

				if (virtualCurrencyTotal.Status == ApiResponse.CREEL_PENDING) {
					if (!shown) {
						shown = true;
						Showmessage();
					} 
					return false;
				} else if (virtualCurrencyTotal.Status == ApiResponse.CREEL_DEAUTHORIZE) {
					if (!shown) {
						shown = true;
						Showmessage();
					}
					return false;
				} else {
					shown = false;
					if (TreasureHunt_Globals.getCreel) {
						try {
							var x = await ApiCallsExtendtion.Instance.Authenticate (new LogInModel { UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid) }, false, context);
							if (x.Status == ApiResponse.OK && x.Data != null) {
								TreasureHunt_Globals.Customerid = Guid.Parse (x.Data.ToString ());
								if (TreasureHunt_Globals.Customerid == Guid.Empty) {
									TreasureHunt_Globals.getCreel = false;
								}
							}
						} catch (Exception e) {
							ApiCallsExtendtion.Instance.Error_Send (e);
						}
					}
					return true;
				}
			} catch (Exception e) {
				return false;
			}
		}

		public async Task<bool> Showmessage(){
			var Yes = await context.ShowYesOrNoDialog (virtualCurrencyTotal.ErrorString, context.GetText(Resource.String.yes), context.GetText(Resource.String.no));
			if (Yes) {
				//browser
				ApiCallsExtendtion.Instance.LoginCreel (lmodel, false, context);
				var uri = Android.Net.Uri.Parse (context.GetText(Resource.String.creelsandbox));
				var intent = new Intent (Intent.ActionView, uri);
				context.StartActivity (intent);
				TreasureHunt_Globals.getCreel = true;
				return false;
			} else {
				context.Finish ();
				return false;
			}
		}
	}
}

