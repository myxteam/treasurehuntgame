﻿using System;
using PerpetualEngine.Storage;
using TreasureGameModels;
using TreasureHuntLib;

namespace TresureHunt
{
	public class TreasureHunt_Globals
	{
		const string SILVERSTRING = "SILVER_", GOLDSTRING = "GOLD_", PEARLSTRING = "PEARL_", DIAMONDSTRING = "DIAMOND_",
		GUESTSTRING = "Guest54bd653";
		public static string TextValueString = "Gold";
		const string UserGuidAuth = "UserGUid";
		public static SimpleStorage storage;
		public static TreasureContains treasureContain = TreasureContains.GoldCoins;
		public static int uniqueId = 0,inch = 0;
		public static string registrationId = "";
		public static Guid Customerid;
		public static LoginAndRegistrationEnum.Page pageEnumsValue = LoginAndRegistrationEnum.Page.Login;
		public static bool getCreel =true;
        public static bool forExchange = false;
		public static bool isLoggedIN
		{
			get
			{
				if (!(String.IsNullOrEmpty(userguid)))
				{
					return true;
				}
				else
				{
					return false;
				}

			}
		}

		/// <summary>
		/// Also Know As gold Variable Change it to Value Currency
		/// adjusted so that just by setting the Treasure contain to gold and silver the Code will 
		/// automatically adjust.
		/// </summary>
		static int goldCurrency;
        public static int GoldCurrency
		{
			get
			{
				return goldCurrency;
			}

			set
			{
				goldCurrency = value;
			}
		}


		private static int _cost;
		public static int COST_SOFT
		{
			get
			{
				return _cost;
			}
		}

		public static int Cost
		{
			get
			{

				switch (_cost)
				{
				case 0: { _cost = 1; return _cost; }
				case 1: { _cost = 2; return _cost; }
				case 2: { _cost = 3; return _cost; }
				case 3: { _cost = 5; return _cost; }
				case 5: { _cost = 8; return _cost; }
				case 8: { _cost = 10; return _cost; }
				case 10: { _cost = 12; return _cost; }
				case 12: { _cost = 15; return _cost; }
				case 15: { _cost = 20; return _cost; }
				case 20: { _cost = 25; return _cost; }
				case 25: { _cost = 30; return _cost; }
				}
				 return _cost;
			}

			set
			{
				_cost = value;
			}
		}
		static string deviceId;

		public static string DeviceId {
			get {
				deviceId = storage.Get<string>("InstallationId") ?? "";
				if (String.IsNullOrEmpty(deviceId))
				{
					deviceId = Guid.NewGuid()+"";
					storage.Put<string>("InstallationId", deviceId);
				}
				return deviceId;
			}
		}

		static string creelUserName;

		public static string CreelUserName {
			get {
				creelUserName = storage.Get<string>("creelUserName") ?? "";
				return creelUserName;
			}
			set{
				creelUserName = value;
				storage.Put<string>("creelUserName", creelUserName);
			}
		}
		static string username;
		public static string Username
		{
			get
			{
				username = storage.Get<string>("USERNAME") ?? null;
				return username;
			}
			set
			{
				username = value;
				                if (String.IsNullOrEmpty(username))
				                {
				                    if (storage.HasKey("USERNAME"))
				                    {
				                        storage.Delete("USERNAME");
				                    }
				                }
				                else
				                {
				                    storage.Put<string>("USERNAME", username);
				                }
			}
		}

		static string password;
		public static string Password
		{
			get
			{
				password = storage.Get<string>("PASSWORD") ?? null;
				return password;
			}
			set
			{
				password = value;
			}
		}

		static string userguid;
		public static string Userguid
		{
			get
			{
				userguid = storage.Get<string>(UserGuidAuth);
				return userguid;
			}
			set
			{
				userguid = value;
				{
					storage.Put<string>(UserGuidAuth, userguid);
				}

			}
		}




		static int currentSession;
		public static int CurrentSession
		{
			get
			{
				currentSession = storage.Get<int>("CURRENT_SESSION");
				return currentSession;
			}
			set
			{
				currentSession = value;
				storage.Put<int>("CURRENT_SESSION", currentSession);
			}
		}

		static Guid gameId;
		public static Guid GameId
		{
			get
			{

				if (treasureContain == TreasureContains.GoldCoins)
				{
					return Guid.Parse("57bca793-2632-4f0e-a7a8-031b3b83f6dd");
				}
				return Guid.Parse("081bd80a-a53e-4002-8793-c89957db71b8");
			}

		}
	}
}

