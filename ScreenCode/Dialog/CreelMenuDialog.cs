using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using PerpetualEngine.Storage;
using TreasureGameModels;
using System.Threading.Tasks;
using Android.Net;
using Android.Media;
using Android.Util;
using TresureHunt.ScreenCode;
using TresureHunt.ScreenCode.Dialog;

namespace TresureHunt.ScreenCode.Dialog
{
    public class CreelMenuDialog : DialogFragment
    {
        private Button btnCreelMenuTrans, btnCreelMenuSend, btnCreelMenuLink, btnCreelBack;
		LinearLayout rootLInearLayout;
		CreelAuthenticationHandler creelAuthenticationHandler;
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            base.OnCreateView(inflater, container, savedInstanceState);
            var view = inflater.Inflate(Resource.Layout.CreelMenu, container, false);

            btnCreelMenuLink = view.FindViewById<Button>(Resource.Id.linkCreelbtn);
            btnCreelMenuTrans = view.FindViewById<Button>(Resource.Id.transferCreelbtn);
            btnCreelMenuSend = view.FindViewById<Button>(Resource.Id.sendCreelbtn);
			btnCreelBack = view.FindViewById<Button> (Resource.Id.btnCreelBack);
			rootLInearLayout = view.FindViewById<LinearLayout> (Resource.Id.rootLInearLayout);

			rootLInearLayout.SetBackgroundResource (Resource.Drawable.portfolio_popup_panel);
			if (TreasureHunt_Globals.CreelUserName == string.Empty)
            {
                btnCreelMenuTrans.Enabled = false;
                btnCreelMenuSend.Enabled = false;
            }
            else
            {
                btnCreelMenuTrans.Enabled = true;
                btnCreelMenuSend.Enabled = true;
				btnCreelMenuLink.Text = GetText(Resource.String.unlinkcreel);
            }

            btnCreelMenuLink.Click += btnCreelMenuLink_Click;
            btnCreelMenuTrans.Click += btnCreelMenuTrans_Click;
            btnCreelMenuSend.Click += btnCreelMenuSend_Click;
			btnCreelBack.Click += BtnCreelBack_Click;
            
            return view;
        }

        void BtnCreelBack_Click (object sender, EventArgs e)
        {
			this.Dismiss();
        }

        async void btnCreelMenuSend_Click(object sender, EventArgs e)
        {
				TreasureHunt_Globals.getCreel = true;
				this.Activity.StartActivity(typeof(CreelUserTansferActivity));
				this.Dismiss ();
        }

        void btnCreelMenuTrans_Click(object sender, EventArgs e)
        {
			TreasureHunt_Globals.getCreel = true;
            this.Activity.StartActivity(typeof(CreelTransferActivity));
			this.Dismiss ();
        }

        async void btnCreelMenuLink_Click(object sender, EventArgs e)
        {
			var lmodel = new LogInModel();
			lmodel.CreelUserName = TreasureHunt_Globals.CreelUserName;
			lmodel.DeviceId = TreasureHunt_Globals.DeviceId;
			lmodel.GameID = Guid.NewGuid();
			lmodel.UserName = TreasureHunt_Globals.Username;
			lmodel.Password = TreasureHunt_Globals.Password;
			lmodel.UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid);
			creelAuthenticationHandler = new CreelAuthenticationHandler(lmodel,(this.Activity as BaseActivity));
          
			var Response = await creelAuthenticationHandler.CheckReauthorize();
            if (TreasureHunt_Globals.CreelUserName == string.Empty && TreasureHunt_Globals.Customerid == Guid.Empty )
            {
					this.Activity.StartActivity (typeof(CreelLinkActivity));
					this.Dismiss ();
            }
            else
            {
				
                if (Response)
                {
                    String device = Build.Model;
					string OS = GetText(Resource.String.osmodel) + Build.VERSION.Sdk;
                    string manufacturer = Build.Manufacturer;

                    var x = await ApiCallsExtendtion.Instance.UnRegisterCreel(new LogInModel
                    {
                        UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid),
                        DeviceId = TreasureHunt_Globals.DeviceId,
                        DeviceName = device,
                        Manufacturer = manufacturer,
                        Model = OS
                    }, true, (Activity as BaseActivity));

                    if (x.Status == ApiResponse.OK)
                    {
                        (Activity as BaseActivity).ShowOKDialog(x.Data.ToString());
                        TreasureHunt_Globals.Customerid = Guid.Empty;
                        TreasureHunt_Globals.CreelUserName = string.Empty;
                    }
                    else if (x.ErrorString != string.Empty)
                    {
						(Activity as BaseActivity).ShowOKDialog(GetText(Resource.String.unlinkcantcomplete) + x.ErrorString);
                    }
                    else
                    {

                        (Activity as BaseActivity).ShowOKDialog(GetText(Resource.String.erroroccured));
                    }

                    this.Dismiss();
                }
            }            
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);
            base.OnActivityCreated(savedInstanceState);
        }
		public override void OnDismiss (IDialogInterface dialog)
		{
			base.OnDismiss (dialog);		

			if(btnCreelMenuTrans !=null)
			{
				btnCreelMenuTrans.SetBackgroundResource(0);
				btnCreelMenuTrans.Dispose();
				btnCreelMenuTrans=null;
			}
			if(btnCreelMenuSend !=null)
			{
				btnCreelMenuSend.SetBackgroundResource(0);
				btnCreelMenuSend.Dispose();
				btnCreelMenuSend=null;
			}
			if(btnCreelMenuLink !=null)
			{
				btnCreelMenuLink.SetBackgroundResource(0);
				btnCreelMenuLink.Dispose();
				btnCreelMenuLink=null;
			}
			if(btnCreelBack !=null)
			{
				btnCreelBack.SetBackgroundResource(0);
				btnCreelBack.Dispose();
				btnCreelBack=null;
			}
			if(rootLInearLayout !=null)
			{
				rootLInearLayout.SetBackgroundResource(0);
				rootLInearLayout.Dispose();
				rootLInearLayout=null;
			}
		}
    }
}