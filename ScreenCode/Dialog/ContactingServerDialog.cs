﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TresureHunt
{
	public class ContactingServerDialog : DialogFragment
	{

		public ContactingServerDialog(string message)
		{
		}

		public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
			this.Cancelable = false;
        }

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);
			try{
			var view = inflater.Inflate(Resource.Layout.LoadingMessage, container, false);
			this.Cancelable = false;
			this.Dialog.SetCanceledOnTouchOutside (false);
				return view;
			}catch{
				return null;
			}
		}
		public override void OnDismiss (IDialogInterface dialog)
		{
			base.OnDismiss (dialog);
			try{
			this.View.FindViewById<ImageView> (Resource.Id.imageView1).SetBackgroundResource (0);
			this.View.FindViewById<LinearLayout> (Resource.Id.linearLayout1).SetBackgroundResource (0);
			}catch{
			}
		}
		public override void OnDestroyView ()
		{
			base.OnDestroyView ();

		}

	}
}

