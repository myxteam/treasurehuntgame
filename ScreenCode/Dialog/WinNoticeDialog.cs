﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using TreasureGame;

namespace TresureHunt
{
	public class WinNoticDialog : DialogFragment
	{
		ChestType chestType;
        int Ammount;

        bool isText;
        String Text;

		public WinNoticDialog(ChestType chestType, int ammount, bool _isText, String Text){
			this.chestType = chestType;
            this.Ammount = ammount;
            this.isText = _isText;
            this.Text = Text;
		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}
		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);
			var view = inflater.Inflate(Resource.Layout.WinNoticeDialog, container, false);
			var textView = view.FindViewById<TextView> (Resource.Id.tVNotice);

            if (isText)
            {
				textView.Text = GetText(Resource.String.notenoughgold);
            }
            else
            {

                switch (chestType)
                {
                    case ChestType.Closed_PirateChest_Empty:
					textView.Text = GetText(Resource.String.sorrytryagain);
                        break;
                    case ChestType.Closed_PirateChest_Gold:
					textView.Text = GetText(Resource.String.noticMsg) + " " + Ammount + " " + GetText(Resource.String.gold);
                        break;
                    default:
					textView.Text = GetText(Resource.String.chestalreadyopen);
                        break;
                }
            }
			return view;
		}
		public override void OnDetach ()
		{
			base.OnDetach ();
		}
		public override void OnDismiss (IDialogInterface dialog)
		{
			base.OnDismiss (dialog);
			chestType = ChestType.Open_PirateChest_Empty;
		}
	}
}

