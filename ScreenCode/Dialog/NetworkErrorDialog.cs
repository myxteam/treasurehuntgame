﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace TresureHunt
{
	public class NetworkErrorDialog : DialogFragment
	{
		public event EventHandler YesEvent, NoEvent;
		string message;

		public NetworkErrorDialog (string message)
		{
			this.message = message ?? string.Empty;
		}

		public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			base.OnCreateView (inflater, container, savedInstanceState);
			try{
			var view = inflater.Inflate(Resource.Layout.YesNoDialog, container, false);
			view.FindViewById<TextView>(Resource.Id.txtMessage).Text = message;
			var btnYes = view.FindViewById<Button>(Resource.Id.btnYes);
			btnYes.Click += Yes_Click;
			var btnNo = view.FindViewById<Button>(Resource.Id.btnNo);
			btnNo.Click += No_Click;

			return view;
			}
			catch{
				return null;
			}
		}

		void Yes_Click (object sender, EventArgs e)
		{
			if (this.YesEvent != null) 
			{
				YesEvent(this, null);
			}
		}

		void No_Click (object sender, EventArgs e)
		{
			if (this.NoEvent != null) 
			{
				NoEvent(this, null);
			}
		}
	}
}

