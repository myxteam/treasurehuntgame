﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using System.Threading.Tasks;
using Android.Graphics;
using Android.Util;
using PerpetualEngine.Storage;
using Newtonsoft.Json;
using TreasureGameModels;
using System.Diagnostics;
using System.Threading;

namespace TresureHunt
{
	[Activity (Label = "BaseActivity",Theme = "@android:style/Theme.NoTitleBar.Fullscreen",ScreenOrientation = ScreenOrientation.SensorLandscape)]			
	public class BaseActivity : Activity
	{
		ContactingServerDialog loadingDialog; 
		NetworkErrorDialog errorDialog;
		protected Typeface parchment;
		protected Typeface mariana;
		protected int layoutId = 0;
		LayoutManager layoutManager;
        WalletModel wallet;

		protected override void OnCreate (Bundle bundle)
		{
            try
            {
                base.OnCreate(bundle);
                layoutManager = new LayoutManager();
                wallet = null;
                SimpleStorage.SetContext(ApplicationContext);
                TreasureHunt_Globals.storage = SimpleStorage.EditGroup(GetText(Resource.String.info));
                var inch = TreasureHunt_Globals.storage.Get<string>(GetText(Resource.String.inch));
                if (inch == null || inch == string.Empty)
                {
                    DisplayMetrics dm = new DisplayMetrics();
                    WindowManager.DefaultDisplay.GetMetrics(dm);
                    int widthPixels = dm.WidthPixels;
                    int heightPixels = dm.HeightPixels;
                    double x = Math.Pow(widthPixels / dm.Xdpi, 2);
                    double y = Math.Pow(heightPixels / dm.Ydpi, 2);
                    double screenInches = Math.Round(Math.Sqrt(x + y));
                    TreasureHunt_Globals.storage.Put(GetText(Resource.String.inch), screenInches + string.Empty);
                    TreasureHunt_Globals.inch = Convert.ToInt32(screenInches);
                }
                else
                {
                    TreasureHunt_Globals.inch = Convert.ToInt32(inch);
                }
                layoutId = layoutManager.getLayout(this);
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
		}
		protected override void OnStart ()
		{
            try
            {
                base.OnStart();
                if (TreasureHunt_Globals.DeviceId == null || TreasureHunt_Globals.DeviceId == string.Empty)
                {
					
                }
                if (parchment == null || mariana == null)
                    parchment = Typeface.CreateFromAsset(Assets, GetText(Resource.String.parchment));
				mariana = Typeface.CreateFromAsset(Assets, GetText(Resource.String.mariana));
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);                
            }
		}
		protected override void OnPause ()
		{       
			base.OnPause();
		}
		
		protected override void OnStop()
		{
			base.OnStop();
		}

		protected override void OnRestart ()
		{
			base.OnRestart();
		}

		protected override void OnResume()
		{
			base.OnResume();
		}
		
		protected override void OnDestroy ()
		{
			base.OnDestroy();
			if (layoutManager != null) {
				layoutManager = null;
			}
		}

		public void ShowLoadingDialog (string message)
		{
			var transaction = FragmentManager.BeginTransaction();
			loadingDialog = new ContactingServerDialog(message);
			loadingDialog.SetMenuVisibility(false);
			loadingDialog.SetStyle(DialogFragmentStyle.NoFrame, 0);
			loadingDialog.SetHasOptionsMenu(false);
			loadingDialog.Cancelable = false;
			this.RunOnUiThread(() => {
				loadingDialog.Show(transaction, GetText(Resource.String.contactServerDialog));
			});
		}

        public async Task<WalletModel> FillWallet(bool showMessage)
        {
			try{
			LogInModel loginModel = new LogInModel ();
			loginModel.UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid);
            var result = await ApiCallsExtendtion.Instance.GetOverAllWalletBalnce(loginModel, showMessage, this);
			return JsonConvert.DeserializeObject<WalletModel> (result.Data.ToString());
			}catch(Exception e){
				ApiCallsExtendtion.Instance.Error_Send (e);
				ShowOKDialog (GetText (Resource.String.erroroccured));
				return null;
			}
		}


        public async Task<CoinsModel> UpdateCoinTotal(bool showMessage = false)
        {
			try{
            CoinsModel coin = new CoinsModel();
            wallet = await FillWallet(showMessage);
            coin.gold = Convert.ToInt32(wallet.Currencies.Where(t => t.CurrencyName == GameCurrenciesConstant.GOLD).FirstOrDefault().Amount);
            return coin;
			}catch(Exception e){
				ApiCallsExtendtion.Instance.Error_Send (e);
				ShowOKDialog (GetText (Resource.String.erroroccured));
				return null;
			}
        }
        public async Task<bool> ShowNetworkErrorDialog(string message)
        {
            bool finished = false;
            bool re = false;
            try
            {
                //set alert for executing the task
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.SetMessage(message);
				alert.SetTitle(GetText(Resource.String.arr));

				alert.SetPositiveButton(GetText(Resource.String.yes), (senderAlert, args) =>
                {
                    re = true;
                    finished = true;        
                });

				alert.SetNegativeButton(GetText(Resource.String.no), (senderAlert, args) =>
                {
                    re = false;
                    finished = true;
                });

                RunOnUiThread(() =>
                {
						try{
							alert.Show();
						}catch{}

                });
             while(! finished)
             {
                await Task.Delay(100);
             }
            }
			catch (Exception e){
				ApiCallsExtendtion.Instance.Error_Send (e);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
            return re;
        }


        public async Task<bool> ShowOKDialog(string message)
        {
            bool finished = false;
            bool re = false;
            try
            {
                //set alert for executing the task
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetCancelable(false);
                alert.SetTitle(message);

				alert.SetPositiveButton(GetText (Resource.String.ok), (senderAlert, args) =>
                {
                    re = true;
                    finished = true;
                });

                RunOnUiThread(() =>
                {
                    alert.Show();

                });
                while (!finished)
                {
                    await Task.Delay(100);
                }
            }
            catch (Exception e2) {
				ApiCallsExtendtion.Instance.Error_Send (e2);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
            return re;
        }
		public async Task<bool> ShowYesOrNoDialog(string message, string positiveButtonMessage, string negativeButtonMessage)
		{
			bool finished = false;
			bool re = false;
			try
			{
				//set alert for executing the task
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetCancelable(false);
				alert.SetTitle(message);

				alert.SetPositiveButton(positiveButtonMessage, (senderAlert, args) =>
					{
						re = true;
						finished = true;
						alert.Dispose();
					});
                alert.SetNegativeButton(negativeButtonMessage, (senderAlert, args) =>
					{
						re = false;
						finished = true;
					});
				
				RunOnUiThread(() =>
					{
						alert.Show();

					});
				while (!finished)
				{
					await Task.Delay(100);
				}
			}
			catch (Exception e2) { 
				ApiCallsExtendtion.Instance.Error_Send (e2);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
		
			return re;
		}
		public async Task<int> ShowThreeButton(string message, string positiveButtonMessage, string negativeButtonMessage, string neutralButtonMessage)
		{
			bool finished = false;
			int re = 0;
			try
			{
				//set alert for executing the task
				AlertDialog.Builder alert = new AlertDialog.Builder(this);
				alert.SetCancelable(false);
				alert.SetTitle(message);

				alert.SetPositiveButton(positiveButtonMessage, (senderAlert, args) =>
					{
						re = 1;
						finished = true;
					});
				alert.SetNegativeButton(negativeButtonMessage, (senderAlert, args) =>
					{
						re = 0;
						finished = true;
					});
				alert.SetNeutralButton(neutralButtonMessage, (senderAlert, args) =>
					{
						re = 2;
						finished = true;
					});
				RunOnUiThread(() =>
					{
						alert.Show();

					});
				while (!finished)
				{
					await Task.Delay(100);
				}
			}
			catch (Exception e2) { 
				ApiCallsExtendtion.Instance.Error_Send (e2);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
			return re;
		}
		public void DismissLoadingDialog()
		{
			if (loadingDialog != null)
				loadingDialog.Dismiss ();
		}

		public void DismissErrorDialog ()
		{
			if(loadingDialog != null && loadingDialog.IsVisible)	
				errorDialog.Dismiss();
		}

        public void DisposeWidget(Button button = null, ImageButton imageButton = null, TextView textview = null, ImageView imageView = null, RelativeLayout relativeLayout = null)
        {
            if (button != null)
            {
                button.SetBackgroundResource(0);
                button = null;
            }
            if (imageButton != null)
            {
                imageButton.SetBackgroundResource(0);
                imageButton = null;
            }
            if (textview != null)
            {
                textview.SetBackgroundResource(0);
                textview = null;
            }
            if (imageView != null)
            {
                imageView.SetBackgroundResource(0);
                imageView.SetImageResource(0);
                imageView = null;
            }
            if (relativeLayout != null)
            {
                relativeLayout.SetBackgroundResource(0);
                relativeLayout = null;
            }
        }
		public void DisposeWidgetLinearLayout(LinearLayout linearLayout= null){
			if (linearLayout != null)
			{
				linearLayout.SetBackgroundResource(0);
				linearLayout = null;
			}
		}
		public void DisposeWidgetEditText(EditText editText= null){
			if (editText != null)
			{
				editText = null;
			}
		}
	}
}

