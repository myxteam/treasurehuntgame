using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using TreasureGameModels;
using Newtonsoft.Json;
using Creel.Common.Data;
using System.Threading.Tasks;

namespace TresureHunt.ScreenCode
{
	[Activity (Label = "UserTansfer", Theme = "@android:style/Theme.NoTitleBar.Fullscreen", ScreenOrientation = ScreenOrientation.SensorLandscape)]
	public class CreelUserTansferActivity : BaseActivity
	{
		Button btnBack, btnSend, btnDedication, btnOk;
		AutoCompleteTextView aCUserName;
		EditText eTAmount, eTDedicationMessage;
		TextView tVUserName, tVAmount, tvCreelAmount;
		LinearLayout rootLinearLayout, lLMessageHolder;
		List<CustomerAccount> customerAccounts;
		List<string> CustomerNames;
		ArrayAdapter autoCompleteAdapter;
		ApiResult virtualCurrencyTotal = null;
		string message = string.Empty;
		bool shown = false, search = true;
		LogInModel lmodel;
		CreelAuthenticationHandler creelAuthenticationHandler;

		protected override void OnCreate (Bundle bundle)
		{
			try {
				base.OnCreate (bundle);
				SetContentView (layoutId);

				btnBack = FindViewById<Button> (Resource.Id.btnBack);
				btnSend = FindViewById<Button> (Resource.Id.btnSend);
				btnDedication = FindViewById<Button> (Resource.Id.btnDedication);
				btnOk = FindViewById<Button> (Resource.Id.btnOk);
				aCUserName = FindViewById<AutoCompleteTextView> (Resource.Id.aCUserName);
				eTAmount = FindViewById<EditText> (Resource.Id.eTAmount);
				eTDedicationMessage = FindViewById<EditText> (Resource.Id.eTDedicationMessage);
				tVUserName = FindViewById<TextView> (Resource.Id.tVUserName);
				tVAmount = FindViewById<TextView> (Resource.Id.tVAmount);
				rootLinearLayout = FindViewById<LinearLayout> (Resource.Id.rootLinearLayout);
				lLMessageHolder = FindViewById<LinearLayout> (Resource.Id.lLMessageHolder);
				tvCreelAmount = FindViewById<TextView> (Resource.Id.TVCreelAmount);

				if (customerAccounts == null) {
					customerAccounts = new List<CustomerAccount> ();
				}
				if (CustomerNames == null) {
					CustomerNames = new List<string> ();
				}

				aCUserName.Threshold = 1;
				aCUserName.TextChanged += acTCreelID_TxtChange;

				btnSend.Click += BtnSend_Click;
				btnDedication.Click += BtnDedication_Click;
				btnOk.Click += BtnOk_Click;
				btnBack.Click += backBtn_Click;
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
		
		}

		protected override async void OnStart ()
		{
			try {
				base.OnStart ();

				tVAmount.Text = tVAmount.Text.ToLower ();
				tVUserName.Text = tVUserName.Text.ToLower ();
				btnSend.SetBackgroundResource (Resource.Drawable.CompleteFlagButton);
				btnSend.SetTextColor (Android.Graphics.Color.White);
				btnBack.SetBackgroundResource (Resource.Drawable.CompleteFlagButton);
				btnBack.SetTextColor (Android.Graphics.Color.White);
				btnDedication.SetBackgroundResource (Resource.Drawable.CompleteFlagButton);
				btnDedication.SetTextColor (Android.Graphics.Color.White);
				btnOk.SetBackgroundResource (Resource.Drawable.CompleteFlagButton);
				btnOk.SetTextColor (Android.Graphics.Color.White);

				lmodel = new LogInModel ();
				lmodel.UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid);
				lmodel.UserName = TreasureHunt_Globals.Username;
				lmodel.Password = TreasureHunt_Globals.Password;
				lmodel.CreelUserName = TreasureHunt_Globals.CreelUserName;
				lmodel.DeviceId = TreasureHunt_Globals.DeviceId;
				creelAuthenticationHandler = new CreelAuthenticationHandler(lmodel,this);

				var UpdateCoins = await UpdateCoinTotal (false);
				tvCreelAmount.Text = Convert.ToInt32 (UpdateCoins.gold) + string.Empty;

				if (TreasureHunt_Globals.CreelUserName != string.Empty) {
					await creelAuthenticationHandler.CheckReauthorize ();
				}		
				reInitializeAdapter ();


				virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetCurrentCapcity_Creel (new TransferModel {
					CreelGuid = TreasureHunt_Globals.Customerid,
					DeviceID = TreasureHunt_Globals.DeviceId
				}, true, this);
			} catch (Exception e) {
				ApiCallsExtendtion.Instance.Error_Send (e);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
		}

		void BtnOk_Click (object sender, EventArgs e)
		{
			message = eTDedicationMessage.Text;
			lLMessageHolder.Visibility = ViewStates.Gone;
		}

		void BtnDedication_Click (object sender, EventArgs e)
		{
			if (lLMessageHolder.Visibility == ViewStates.Gone) {
				lLMessageHolder.Visibility = ViewStates.Visible;
			}
		}

		private async void acTCreelID_TxtChange (object sender, Android.Text.TextChangedEventArgs e)
		{
			await Task.Delay (5000);
			try {
				if (search) {
					search = false;
					var authorize = await creelAuthenticationHandler.CheckReauthorize ();
					if (authorize) {
						var creelServerResponse = await ApiCallsExtendtion.Instance.UserSearch (new UserSearchCreel { CurrentCustomerAccountId = TreasureHunt_Globals.Customerid,
							SearchKeyword = e.Text.ToString (), DeviceId = TreasureHunt_Globals.DeviceId
						}, false, this);
						if (creelServerResponse.Data != null) {
							customerAccounts.Clear ();
							CustomerNames.Clear ();				
							customerAccounts = JsonConvert.DeserializeObject<List<CustomerAccount>> (creelServerResponse.Data.ToString ());
							foreach (var str in customerAccounts) {
								CustomerNames.Add (str.DisplayName);
							}
							reInitializeAdapter ();
							autoCompleteAdapter.NotifyDataSetChanged ();
							search = true;
						} else {
						}
					}
				}
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}

		}

		void reInitializeAdapter ()
		{
			autoCompleteAdapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleDropDownItem1Line
				, CustomerNames.ToArray ());
			aCUserName.Adapter = autoCompleteAdapter;
		}

		void backBtn_Click (object sender, EventArgs e)
		{
			Finish ();
		}

		async void BtnSend_Click (object sender, EventArgs e)
		{
			try {
				string toUser = aCUserName.Text.Trim ();
				int ammount = 0;
				Guid customerFound;
				int.TryParse (eTAmount.Text.Trim (), out ammount);

				if (String.IsNullOrEmpty (toUser) || ammount <= 0) {
					await ShowOKDialog (GetText (Resource.String.pleaseFillOutForm));
					return;
				}

				try {
					var authorize = await creelAuthenticationHandler.CheckReauthorize ();
					if (authorize) {
						TransferToUserModel tUM = new TransferToUserModel ();
						tUM.USER_GUID = Guid.Parse (TreasureHunt_Globals.Userguid);
						tUM.FROM_CREEL_GUID = TreasureHunt_Globals.Customerid;
						tUM.TO_CREEL_GUID = customerAccounts.Where (c => c.DisplayName.Equals (toUser)).FirstOrDefault ().CustomerAccountId;
						tUM.Amount = Convert.ToInt32 (eTAmount.Text);
						tUM.GameID = TreasureHunt_Globals.GameId;
						tUM.DeviceID = TreasureHunt_Globals.DeviceId;
						tUM.Message = message;
						if (Convert.ToDecimal (eTAmount.Text) <= Convert.ToDecimal (tvCreelAmount.Text)) {
							var x = await ApiCallsExtendtion.Instance.TransferToUser_Real (tUM, true, this);

							if (x.Status == ApiResponse.OK && x.Data != null) {
								ShowOKDialog (x.Data.ToString ());
								var UpdateCoins = await UpdateCoinTotal (true);

								aCUserName.Text = string.Empty;
								eTAmount.Text = string.Empty;

								if (UpdateCoins.gold != null) {
									tvCreelAmount.Text = Convert.ToInt32 (UpdateCoins.gold) + string.Empty
										;
								} else {
									ShowOKDialog (x.ErrorString);
									return;
								}
							}
						} else {
							ShowOKDialog (GetText(Resource.String.insufficientbalance));
						}
					}
				} catch (Exception ex) {
					ApiCallsExtendtion.Instance.Error_Send (ex);
					ShowOKDialog (GetText(Resource.String.usernotfound));
				}
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
				ShowOKDialog (GetText (Resource.String.erroroccured));
			}
		}



		protected override async void OnResume ()
		{
			base.OnResume ();
			shown = false;
			search = true;
			creelAuthenticationHandler.CheckReauthorize ();

		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			DisposeWidget (btnSend);
			DisposeWidget (btnBack);
			DisposeWidget (btnOk);
			DisposeWidgetEditText (eTDedicationMessage);
			DisposeWidgetLinearLayout (rootLinearLayout);
			DisposeWidgetLinearLayout (lLMessageHolder);
			if (customerAccounts != null) {
				customerAccounts.Clear ();
				customerAccounts = null;
			}
			if (CustomerNames != null) {
				CustomerNames.Clear ();
				CustomerNames = null;
			}
		}
	}
}