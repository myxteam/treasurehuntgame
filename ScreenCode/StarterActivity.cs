using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using PerpetualEngine.Storage;
using TreasureGameModels;
using System.Threading.Tasks;
using Android.Net;
using Android.Media;
using Android.Util;
using TresureHunt.ScreenCode;
using TresureHunt.ScreenCode.Dialog;
using Newtonsoft.Json;

namespace TresureHunt
{
	[Activity (Label = "Treasure Hunt", MainLauncher = true, Icon = "@drawable/thICON2_60", Theme = "@android:style/Theme.NoTitleBar.Fullscreen", ScreenOrientation = ScreenOrientation.SensorLandscape)]
	public class StarterActivity : BaseActivity
	{
		Button btnStart, btnPorfolio, btnLogin, btnQuit, btnCreelMenu;
		LinearLayout rootLinearLayout;
		TextView tv1;
		public static MediaPlayer _player;
		bool startCLicked = false;

		protected async override void OnCreate (Bundle bundle)
		{
			try {
				base.OnCreate (bundle);
				SetContentView (layoutId);

				//Push Notification
				string senders = "135366940210";
				Intent intent = new Intent ("com.google.android.c2dm.intent.REGISTER");
				intent.SetPackage ("com.google.android.gsf");
				intent.PutExtra ("app", PendingIntent.GetBroadcast (this, 0, new Intent (), 0));
				intent.PutExtra ("sender", senders);
				this.StartService (intent);

				btnStart = FindViewById<Button> (Resource.Id.btnStart);
				btnPorfolio = FindViewById<Button> (Resource.Id.btnPorfolio);
				btnLogin = FindViewById<Button> (Resource.Id.btnLogin);
				btnQuit = FindViewById<Button> (Resource.Id.btnQuit);
				btnCreelMenu = FindViewById<Button> (Resource.Id.creelBtn);

				btnCreelMenu.Visibility = ViewStates.Visible;

				//linearLayout
				rootLinearLayout = FindViewById<LinearLayout> (Resource.Id.rootLinearLayout);
				tv1 = FindViewById<TextView> (Resource.Id.textView1);

				// click events
				btnStart.Click += btnStartClick;
				btnPorfolio.Click += btnPorfolioClick;
				btnLogin.Click += btnLoginClick;
				btnQuit.Click += btnQuit_Click;
				btnCreelMenu.Click += btnCreelMenu_Click;
            
				TreasureHunt_Globals.storage = SimpleStorage.EditGroup (GetText (Resource.String.info));


				GameController.NewPirateTreasureGame (1, 1, false, this);
				string uname = TreasureHunt_Globals.Username;
				if (String.IsNullOrEmpty (uname)) {
					uname = GetText (Resource.String.guess);
				}
				tv1.Text = GetText (Resource.String.welcome) + " " + uname;
			} catch (Exception e) {
				ApiCallsExtendtion.Instance.Error_Send (e);
			}
		}

		private void btnCreelMenu_Click (object sender, EventArgs e)
		{
            try { 
			if (!TreasureHunt_Globals.isLoggedIN) {

				ShowOKDialog (GetText (Resource.String.pleaseLoginFirst));
				return;
			}

			FragmentTransaction transactionCreel = FragmentManager.BeginTransaction ();
			CreelMenuDialog creelMenuDialog = new CreelMenuDialog ();
			creelMenuDialog.SetMenuVisibility (false);
			creelMenuDialog.SetStyle (DialogFragmentStyle.NoFrame, 0);
			creelMenuDialog.SetHasOptionsMenu (false);
			creelMenuDialog.Cancelable = false;
				creelMenuDialog.Show (transactionCreel, GetText(Resource.String.creelmenu));
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }

		}

		void btnQuit_Click (object sender, EventArgs e)
		{
			this.FinishAffinity ();
			Android.OS.Process.KillProcess (Android.OS.Process.MyPid ());
		}

		protected override async void OnStart ()
		{
			try {
				base.OnStart ();

				//set text Value for currency
				SetTextValueString ();

				// putBackground
				btnStart.SetBackgroundResource (Resource.Drawable.THPirateButton);
				btnPorfolio.SetBackgroundResource (Resource.Drawable.THPirateButton);
				btnLogin.SetBackgroundResource (Resource.Drawable.THPirateButton);
				btnQuit.SetBackgroundResource (Resource.Drawable.THPirateButton);
				btnCreelMenu.SetBackgroundResource (Resource.Drawable.CreelButton);

				btnStart.Typeface = mariana;
				btnPorfolio.Typeface = mariana;
				btnLogin.Typeface = mariana;
				btnQuit.Typeface = mariana;
				tv1.Typeface = mariana;

				btnCreelMenu.Visibility = ViewStates.Visible;

				await Task.Delay (100);

				#region new signup for start
                var responseDemoSignup = await ApiCallsExtendtion.Instance.Signup_Demo(new LogInModel() { UserGuid = Guid.Parse(TreasureHunt_Globals.DeviceId), Amount = 200 }, true, this);
				if (responseDemoSignup.Status == ApiResponse.OK)
				{
					TreasureHunt_Globals.Userguid = responseDemoSignup.Data.ToString();
				}
				else
				{
					ShowOKDialog(responseDemoSignup.ErrorString);				
				}
				#endregion

				if (TreasureHunt_Globals.isLoggedIN) {
					btnLogin.Text = GetText (Resource.String.logout);
					if (TreasureHunt_Globals.getCreel) {
						try {
							var x = await ApiCallsExtendtion.Instance.Authenticate (new LogInModel { UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid) }, true, this);
							if (x.Status == ApiResponse.OK && x.Data != null) {
								TreasureHunt_Globals.Customerid = Guid.Parse (x.Data.ToString ());
								if (TreasureHunt_Globals.Customerid == Guid.Empty) {
									TreasureHunt_Globals.getCreel = false;
								}
							}
						} catch (Exception e) {
							ApiCallsExtendtion.Instance.Error_Send(e);
						}
					}
				} else {
					btnLogin.Text = GetText (Resource.String.login);
				}

				var connectivityManager = (ConnectivityManager)GetSystemService (ConnectivityService);
				var activeConnection = connectivityManager.ActiveNetworkInfo;
				if ((activeConnection != null) && activeConnection.IsConnected) {
				} else {
					await ShowOKDialog (GetText (Resource.String.requiresInternetConnection));
					Finish ();
				}
				//Storage Check GUID
				var storage = SimpleStorage.EditGroup (GetText (Resource.String.info));
				if (_player == null) {
					_player = MediaPlayer.Create (this, Resource.Raw.theme2);
				}
				if (!_player.IsPlaying) {
					_player.Start ();
				}
			} catch (Exception e) {
				ApiCallsExtendtion.Instance.Error_Send (e);
			}
		}

		async void btnStartClick (object sender, EventArgs e)
		{
			try {
				if (!startCLicked) {
					startCLicked = true;
					var storage = SimpleStorage.EditGroup (GetText (Resource.String.info));
					if (!TreasureHunt_Globals.isLoggedIN) {
						await ShowOKDialog (GetText (Resource.String.pleaseLoginFirst));
						return;
					}
					if (!GameController.HasGame) {
						await GameController.NewPirateTreasureGame (1, 1, true, this);

					}
					var coinModel = await this.UpdateCoinTotal ();
					TreasureHunt_Globals.GoldCurrency = coinModel.gold;
					StartActivity (typeof(TreasureHuntActivity));
					this.Finish ();
					OverridePendingTransition (Resource.Animation.SlideIn, Resource.Animation.slideOut);
				}
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}
		}

		async void btnPorfolioClick (object sender, EventArgs e)
		{
			if (!TreasureHunt_Globals.isLoggedIN) {
				await ShowOKDialog (GetText (Resource.String.pleaseLoginFirst));
				return;
			}
			
			OverridePendingTransition (Resource.Animation.SlideIn, Resource.Animation.slideOut);
		}

		void btnLoginClick (object sender, EventArgs e)
		{
            try { 
			if (btnLogin.Text == GetText (Resource.String.logout)) {
					TreasureHunt_Globals.Userguid = string.Empty;
				TreasureHunt_Globals.Username = null;
				TreasureHunt_Globals.Password = null;
				TreasureHunt_Globals.Customerid = Guid.Empty;
				TreasureHunt_Globals.getCreel = true;
				StartActivity (typeof(StarterActivity));
				Finish ();
				OverridePendingTransition (Resource.Animation.SlideIn, Resource.Animation.slideOut);
			} else {
				Finish ();
				OverridePendingTransition (Resource.Animation.SlideIn, Resource.Animation.slideOut);
            }
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
		}

		public void SetTextValueString ()
		{
			TreasureHunt_Globals.TextValueString = GetText (Resource.String.gold);                       
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			DisposeWidget (btnStart);
			DisposeWidget (btnLogin);
			DisposeWidget (btnPorfolio);
			DisposeWidgetLinearLayout (rootLinearLayout);
		}
	}
}