using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using TreasureGameModels;
using Newtonsoft.Json;
using Android.Graphics;
using System.Threading.Tasks;

namespace TresureHunt.ScreenCode
{

	[Activity (Label = "TransferActivity", ScreenOrientation = ScreenOrientation.SensorLandscape, Icon = "@drawable/icon", Theme = "@android:style/Theme.NoTitleBar.Fullscreen")]
	public class CreelTransferActivity : BaseActivity
	{
		public static int TRANSFER_AMMOUNT = 10;
		Button btnLeft, btnRight, btnback, btnIncAmt, btnDecAmt;
		TextView GameTV, WalletTV, TVAmm;
		TextView tVTransferableAmount, tVGameAccount, tVCreelAccount, tVSpendingCapacity, tVSpendingCapacity2;
		LinearLayout rootLinearLayout;
		int gold;
		bool clicked = false;
		ApiResult virtualCurrencyTotal = null;
		LogInModel lmodel;
		CreelAuthenticationHandler creelAuthenticationHandler;

		protected override void OnCreate (Bundle bundle)
		{
			try {
				base.OnCreate (bundle);
				SetContentView (layoutId);


				btnLeft = FindViewById<Button> (Resource.Id.btnLeft);
				btnRight = FindViewById<Button> (Resource.Id.btnRight);
				btnback = FindViewById<Button> (Resource.Id.btnBack);
				btnIncAmt = FindViewById<Button> (Resource.Id.btnIncAmount);
				btnDecAmt = FindViewById<Button> (Resource.Id.btnDecAmount);

				rootLinearLayout = FindViewById<LinearLayout> (Resource.Id.rootLinearLayout);


				btnLeft.SetTextColor (Android.Graphics.Color.White);
				btnRight.SetTextColor (Android.Graphics.Color.White);
				btnback.SetTextColor (Android.Graphics.Color.White);

				tVTransferableAmount = FindViewById<TextView> (Resource.Id.tVTransferableAmount);
				tVGameAccount = FindViewById<TextView> (Resource.Id.tVGameAccount);
				tVCreelAccount = FindViewById<TextView> (Resource.Id.tVCreelAccount);
				tVSpendingCapacity = FindViewById<TextView> (Resource.Id.tVSpendingCapacity);
				tVSpendingCapacity2 = FindViewById<TextView> (Resource.Id.tVSpendingCapacity2);
				GameTV = FindViewById<TextView> (Resource.Id.TVGame);
				WalletTV = FindViewById<TextView> (Resource.Id.TVWallet);
				TVAmm = FindViewById<TextView> (Resource.Id.TVAmm);


				if (TreasureHunt_Globals.forExchange == false) {
					btnRight.SetTextColor (Android.Graphics.Color.White);
					btnRight.Visibility = ViewStates.Visible;
					btnIncAmt.Visibility = ViewStates.Visible;
					btnDecAmt.Visibility = ViewStates.Visible;
					tVTransferableAmount.Text = GetText(Resource.String.transferableAmount);
					tVCreelAccount.Text = GetText(Resource.String.creel);
					tVSpendingCapacity.Visibility = ViewStates.Invisible;
					tVSpendingCapacity2.Visibility = ViewStates.Invisible;
					TRANSFER_AMMOUNT = 10;
				} else {
					tVTransferableAmount.Text = GetText(Resource.String.amounttransferred);
					btnRight.Visibility = ViewStates.Invisible;
					btnIncAmt.Visibility = ViewStates.Invisible;
					btnDecAmt.Visibility = ViewStates.Invisible;
					tVCreelAccount.Text = GetText(Resource.String.creelaccount);
					tVSpendingCapacity.Text = GetText(Resource.String.spendingcapacity);
					tVSpendingCapacity2.Visibility = ViewStates.Invisible;
					TRANSFER_AMMOUNT = 30;
				}


				TVAmm.Text = TRANSFER_AMMOUNT.ToString ();
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}
		}

		protected override async void OnStart ()
		{
			try {
				base.OnStart ();

				btnLeft.SetBackgroundResource (Resource.Drawable.LeftFlagButton);
				btnRight.SetBackgroundResource (Resource.Drawable.RightFlagButton);
				btnback.SetBackgroundResource (Resource.Drawable.CompleteFlagButton);
				rootLinearLayout.SetBackgroundResource (Resource.Drawable.bg_treasure_background);

				GameTV.Typeface = parchment;
				WalletTV.Typeface = parchment;
				TVAmm.Typeface = parchment;
				tVTransferableAmount.Typeface = parchment;
				tVGameAccount.Typeface = parchment;
				tVCreelAccount.Typeface = parchment;
				tVSpendingCapacity.Typeface = parchment;
				tVSpendingCapacity2.Typeface = parchment;

				lmodel = new LogInModel ();
				lmodel.CreelUserName = TreasureHunt_Globals.CreelUserName;
				lmodel.DeviceId = TreasureHunt_Globals.DeviceId;
				lmodel.GameID = Guid.NewGuid ();
				lmodel.UserName = TreasureHunt_Globals.Username;
				lmodel.Password = TreasureHunt_Globals.Password;
				lmodel.UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid);
				creelAuthenticationHandler = new CreelAuthenticationHandler(lmodel,this);

				if (TreasureHunt_Globals.CreelUserName != string.Empty) {
					TreasureHunt_Globals.getCreel = true;
					creelAuthenticationHandler.CheckReauthorize ();
					Apicall (lmodel);
				}

				this.DismissLoadingDialog ();
				if (TreasureHunt_Globals.Customerid == Guid.Empty && TreasureHunt_Globals.CreelUserName == string.Empty && TreasureHunt_Globals.forExchange == true) {
					var yesorno = await ShowThreeButton (GetText(Resource.String.dontHaveEnoughGold2), GetText(Resource.String.getfromcreel), GetText(Resource.String.googleplay), GetText(Resource.String.cancel));
					if (yesorno == 1)
						StartActivity (typeof(CreelLinkActivity));
					else if (yesorno == 2) {
						Finish ();
					}
					else 
					{
						await ShowOKDialog(GetText(Resource.String.comingsoon));
						Finish ();
					}
				} else {

					try {
						var coinModel = await UpdateCoinTotal ();
						gold = coinModel.gold;
						this.DismissLoadingDialog ();
					} catch (Exception ex) {
						ApiCallsExtendtion.Instance.Error_Send (ex);
					}

					string tmp = TreasureHunt_Globals.TextValueString;

					btnLeft.Click += btnLeft_Click;
					btnRight.Click += btnRight_Click;
					btnback.Click += btnback_Click;
					btnIncAmt.Click += btnIncAmt_Click;
					btnDecAmt.Click += btnDecAmt_Click;
					btnIncAmt.LongClick += btnIncAmt_LongClick;
					btnDecAmt.LongClick += btnDecAmt_LongClick;

					GameTV.Text = gold.ToString ();
				}

			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}

		}

		public async void Apicall (LogInModel lmodel)
		{
			try {
				if (TreasureHunt_Globals.forExchange == false) {
					virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetCurrentCapcity_Creel (new TransferModel {
						CreelGuid = TreasureHunt_Globals.Customerid,
						DeviceID = TreasureHunt_Globals.DeviceId
					}, true, this);
				} else {
					virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetTotalSpendingCapacity_Creel (new TransferModel {
						CreelGuid = TreasureHunt_Globals.Customerid,
						DeviceID = TreasureHunt_Globals.DeviceId
					}, true, this);
				}
				ApicallResposne ();
			} catch (Exception e) {
				
			}
		}

		public async void ApicallResposne ()
		{
			try {
				if (virtualCurrencyTotal.Data != null) {
					WalletTV.Text = Convert.ToInt32 (virtualCurrencyTotal.Data) + "";
				} else {
					if (virtualCurrencyTotal.ErrorString == string.Empty) {
						ShowOKDialog (GetText(Resource.String.creelserverdown));
					} 
				}
			} catch (Exception e) {

			}
		}

		protected override async void OnResume ()
		{
			base.OnResume ();
			WalletTV.Text = GetText(Resource.String.loading);
			btnLeft.Enabled = false;
			btnRight.Enabled = false;
			if (TreasureHunt_Globals.CreelUserName != string.Empty) {
				//shown = false;
				TreasureHunt_Globals.getCreel = true;
				var authorize = await creelAuthenticationHandler.CheckReauthorize ();
				if (authorize) {
					Apicall (lmodel);
					btnLeft.Enabled = true;
					btnRight.Enabled = true;
					this.DismissLoadingDialog ();
				}
			}
		}

		private void btnDecAmt_LongClick (object sender, View.LongClickEventArgs e)
		{
			if (TRANSFER_AMMOUNT > 10) {
				TRANSFER_AMMOUNT = TRANSFER_AMMOUNT - 50;
				TVAmm.Text = TRANSFER_AMMOUNT.ToString ();
			}
		}

		private void btnIncAmt_LongClick (object sender, View.LongClickEventArgs e)
		{
			if (((TRANSFER_AMMOUNT + 50) <= Convert.ToInt32 (WalletTV.Text)) || ((TRANSFER_AMMOUNT + 50) <= Convert.ToInt32 (GameTV.Text))) {
				TRANSFER_AMMOUNT = TRANSFER_AMMOUNT + 50;
				TVAmm.Text = TRANSFER_AMMOUNT.ToString ();
			}
		}

		private void btnDecAmt_Click (object sender, EventArgs e)
		{
			if (TRANSFER_AMMOUNT > 10) {
				TRANSFER_AMMOUNT = TRANSFER_AMMOUNT - 10;
				TVAmm.Text = TRANSFER_AMMOUNT.ToString ();
			}
		}

		private void btnIncAmt_Click (object sender, EventArgs e)
		{
			if (((TRANSFER_AMMOUNT + 10) <= Convert.ToInt32 (WalletTV.Text)) || ((TRANSFER_AMMOUNT + 10) <= Convert.ToInt32 (GameTV.Text))) {
				TRANSFER_AMMOUNT = TRANSFER_AMMOUNT + 10;
				TVAmm.Text = TRANSFER_AMMOUNT.ToString ();
			}
		}

		void btnback_Click (object sender, EventArgs e)
		{
			TreasureHunt_Globals.forExchange = false;
			Finish ();
		}

		async void btnRight_Click (object sender, EventArgs e)
		{
			try {
				var authorize = await creelAuthenticationHandler.CheckReauthorize ();
				if (authorize) {
					if (!clicked) {
						clicked = true;

						if (gold < TRANSFER_AMMOUNT) {

							await ShowOKDialog (GetString (Resource.String.insufficient) + " " + GetText(Resource.String.gold) + "!");
							clicked = false;
							return;
						}

						var x = await ApiCallsExtendtion.Instance.TransferToWallet (new TransferModel {
							UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid),
							CreelGuid = TreasureHunt_Globals.Customerid,
							Amount = TRANSFER_AMMOUNT,
							DeviceID = TreasureHunt_Globals.DeviceId
						}, true, this);
						if (x != null && x.Status == ApiResponse.OK) {
							await ShowOKDialog (GetText(Resource.String.transfersuccess));
							gold -= TRANSFER_AMMOUNT;
							GameTV.Text = gold.ToString ();
							//clicked = false;					

							if (TreasureHunt_Globals.forExchange == false) {
								virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetCurrentCapcity_Creel (new TransferModel {
									CreelGuid = TreasureHunt_Globals.Customerid,
									DeviceID = TreasureHunt_Globals.DeviceId
								}, true, this);
							} else {
								virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetTotalSpendingCapacity_Creel (new TransferModel {
									CreelGuid = TreasureHunt_Globals.Customerid,
									DeviceID = TreasureHunt_Globals.DeviceId
								}, true, this);
							}

							if (virtualCurrencyTotal.Data != null) {
								WalletTV.Text = Convert.ToInt32 (virtualCurrencyTotal.Data) + "";
								clicked = false;
							} else {
								if (x.ErrorString == string.Empty) {
									await ShowOKDialog (GetText(Resource.String.transactionfailed));
									clicked = false;
									return;
								} else {
									await ShowOKDialog (x.ErrorString);
									clicked = false;
									return;
								}
							}
						} else {
							await ShowOKDialog (x.ErrorString);
							clicked = false;
							return;
						}
					}
				}
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}
			
		}

		async void btnLeft_Click (object sender, EventArgs e)
		{
			try {
				//  TransferToGame
				var authorize = await creelAuthenticationHandler.CheckReauthorize ();
				if (authorize) {
					if (!clicked) {
						clicked = true;

						if (Convert.ToInt32 (WalletTV.Text) > 0) {
							ApiResult x = null;
							if (TreasureHunt_Globals.forExchange == false) {
								x = await ApiCallsExtendtion.Instance.TransferToGame (new TransferModel {
									UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid),
									CreelGuid = TreasureHunt_Globals.Customerid,
									Amount = TRANSFER_AMMOUNT,
									DeviceID = TreasureHunt_Globals.DeviceId
								}, true, this);
							} else {
								x = await ApiCallsExtendtion.Instance.BuyFromCreel (new TransferModel {
									UserGuid = Guid.Parse (TreasureHunt_Globals.Userguid),
									CreelGuid = TreasureHunt_Globals.Customerid,
									Amount = TRANSFER_AMMOUNT,
									DeviceID = TreasureHunt_Globals.DeviceId
								}, true, this);
							}


							if (x != null && x.Status == ApiResponse.OK) {
								var okExchange = await ShowOKDialog (GetText(Resource.String.transfersuccess));
								gold += TRANSFER_AMMOUNT;
								GameTV.Text = gold.ToString ();
								if (okExchange && TreasureHunt_Globals.forExchange) {
									TreasureHunt_Globals.forExchange = false;
									Finish ();
								}

								if (TreasureHunt_Globals.forExchange == false) {
									virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetCurrentCapcity_Creel (new TransferModel {
										CreelGuid = TreasureHunt_Globals.Customerid,
										DeviceID = TreasureHunt_Globals.DeviceId
									}, true, this);
								} else {
									virtualCurrencyTotal = await ApiCallsExtendtion.Instance.GetTotalSpendingCapacity_Creel (new TransferModel {
										CreelGuid = TreasureHunt_Globals.Customerid,
										DeviceID = TreasureHunt_Globals.DeviceId
									}, true, this);
								}

								if (virtualCurrencyTotal.Data != null) {
									WalletTV.Text = Convert.ToInt32 (virtualCurrencyTotal.Data) + "";
									clicked = false;
								} else {
									await ShowOKDialog (GetText(Resource.String.transactionfailed));
									clicked = false;
									return;
								}
								clicked = false;
								return;
							} else {
								await ShowOKDialog (x.ErrorString);
								clicked = false;
								return;
							}
						} else {
							await ShowOKDialog (GetText(Resource.String.insufficientbalance));
						}
					}
				}
			} catch (Exception ex) {
				ApiCallsExtendtion.Instance.Error_Send (ex);
			}
		}

		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			DisposeWidget (btnLeft);
			DisposeWidget (btnRight);
			DisposeWidget (btnback);
			DisposeWidgetLinearLayout (rootLinearLayout);

			if (virtualCurrencyTotal != null) {
				virtualCurrencyTotal = null;
			}
		}
	}
}