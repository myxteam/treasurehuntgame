﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using TreasureGame;
using TreasureGameModels;
using TresureHunt.ScreenCode;
using System.Threading.Tasks;

namespace TresureHunt
{
    [Activity(Label = "TresureHunt", ScreenOrientation = ScreenOrientation.SensorLandscape, Icon = "@drawable/treasure_box_gold_coin_14", Theme = "@android:style/Theme.NoTitleBar.Fullscreen")]
    public class TreasureHuntActivity : BaseActivity
    {
        private LinearLayout linearLayoutHolder;
        private RelativeLayout relativeLayoutMain;
        private TextView tvCost, tVBalance, tVGoldBalance;
        private static int gold, silver, copper;
        private static Button btnMenu, btnNewGame;
        private CoinsModel updatedCoins;
        public Grid grid;
        GridController gridController;
		Random rnd;
		int cost=0, clickctr=0;
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                base.OnCreate(bundle);
                GameController.HasGame = false;
                rnd = new Random();
                TreasureHunt_Globals.Cost = 3;
                GameController.NewPirateTreasureGame(1, 1, false, this);
                updatedCoins = new CoinsModel();
                SetContentView(layoutId);

                #region init layouts
                // init layout elements
                btnMenu = FindViewById<Button>(Resource.Id.btnMenu);
                btnMenu.SetBackgroundResource(Resource.Drawable.treasure_btn);

                btnNewGame = FindViewById<Button>(Resource.Id.btnNewGame);
                btnNewGame.SetBackgroundResource(Resource.Drawable.treasure_btn);

                btnMenu.SetTextColor(Android.Graphics.Color.White);
                btnNewGame.SetTextColor(Android.Graphics.Color.White);

                btnMenu.Typeface = mariana;
                btnNewGame.Typeface = mariana;

                btnNewGame.Visibility = ViewStates.Invisible;

                tvCost = FindViewById<TextView>(Resource.Id.tVCost);
                tVBalance = FindViewById<TextView>(Resource.Id.tVBalance);
                tVGoldBalance = FindViewById<TextView>(Resource.Id.tVGoldBalance);
                linearLayoutHolder = FindViewById<LinearLayout>(Resource.Id.LinearLayoutHolder);
                relativeLayoutMain = FindViewById<RelativeLayout>(Resource.Id.relativeLayoutMain);

                // init grids
                gridController = new GridController(GameController.CurrentGame.Grid);
                gridController.Changed += gridController_Changed;
                gridController.Query += gridController_Query;
                gridController.GameEvent += gridController_GameEvent;

                init();

                //Click Event
                btnMenu.Click += btnMenuClick;
                btnNewGame.Click += btnNewGame_Click;

                #endregion
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
        }

        void btnNewGame_Click(object sender, EventArgs e)
        {
            try
            {
                TreasureHunt_Globals.Cost = 3;
                gridController.DestroyMe();
                gridController = null;

                linearLayoutHolder.RemoveAllViewsInLayout();

                GameController.HasGame = false;
                GameController.NewPirateTreasureGame(1, 1, false, this);

                gridController = new GridController(GameController.CurrentGame.Grid);
                gridController.Changed += gridController_Changed;
                gridController.Query += gridController_Query;
                gridController.GameEvent += gridController_GameEvent;

                TreasureHunt_Globals.Cost = 3;

                init();
                btnNewGame.Visibility = ViewStates.Invisible;
                clickctr = 0;
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
                ShowOKDialog(GetText(Resource.String.erroroccured));
            }
        }
		protected override async void OnStart ()
		{
            try
            {
                base.OnStart();

                tvCost.Text = GetText(Resource.String.costToOpen) + " " + TreasureHunt_Globals.COST_SOFT + " ";
                tvCost.Typeface = mariana;
                tVGoldBalance.Typeface = mariana;
                tVBalance.Typeface = mariana;

                updateTextViews();
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
		}

        protected override async void OnResume()
        {
            base.OnResume();

            updatedCoins = await this.UpdateCoinTotal(true);

			try{
                TreasureHunt_Globals.GoldCurrency = updatedCoins.gold;
			    cost = TreasureHunt_Globals.COST_SOFT;
                tVBalance.Text = TreasureHunt_Globals.GoldCurrency + " "; 
                tVGoldBalance.Text = updatedCoins.gold + " ";
                updateTextViews();
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
        }


        async void gridController_GameEvent(object sender, WinArgs e)
        {
			DismissLoadingDialog ();
            GameEvent model = new GameEvent();
            model.Amount = e.ammont;
			model.Cost = TreasureHunt_Globals.COST_SOFT;
            model.Username = TreasureHunt_Globals.Username;
            model.Password = TreasureHunt_Globals.Password;
            model.UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid);
            model.GameId = TreasureHunt_Globals.GameId;
            model.SessionID = TreasureHunt_Globals.CurrentSession;
			switch (e.treasureContains) {
			    case TreasureHuntLib.TreasureContains.GoldCoins:
				    model.CurrencyID = GameCurrenciesConstant.GOLD_ID;
				    TreasureHunt_Globals.GoldCurrency += e.ammont;
				    break;
			    default:
				    model.CurrencyID = 0;
				    break;
			}
			try{
				var x = await ApiCallsExtendtion.Instance.SendEvent (model, true, this);
				if (x.Status == ApiResponse.OK) {
                    cost = TreasureHunt_Globals.Cost;

					updateTextViews (true);
					tvCost.Text = GetText(Resource.String.costToOpen) + cost;
					return;
				} else {
					return;
				}
			}catch (Exception ex){
				ApiCallsExtendtion.Instance.Error_Send (ex);
				this.ShowOKDialog (GetText (Resource.String.erroroccured));
			}
			
        }

        async void gridController_Query(object sender, EventArgs e)
        {
			var okpress = await ShowOKDialog(GetText(Resource.String.dontHaveEnoughGold));

			if(okpress)
			{
                TreasureHunt_Globals.forExchange = true;
				StartActivity (typeof(CreelTransferActivity));
			}
        }	

        void gridController_Changed(object sender, EventArgs e)
        {
            updateTextViews();
            	
            clickctr++;

            if (clickctr >= 9)
            {
                btnNewGame.Visibility = ViewStates.Visible;
            }
        }

		public async void updateTextViews(bool showMessage = false){

			try{
                updatedCoins = await this.UpdateCoinTotal(showMessage);
            TreasureHunt_Globals.GoldCurrency = updatedCoins.gold;
			tVBalance.Text = TreasureHunt_Globals.GoldCurrency + " ";
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);               
            }   
		}

        
        public void init()
        {
			relativeLayoutMain.SetBackgroundResource(Resource.Drawable.chestcave_bg);
            this.RunOnUiThread(() =>
            {
                gridController.initGame(ref linearLayoutHolder, this, this.FragmentManager);
					tvCost.Text = GetText(Resource.String.costToOpen) + TreasureHunt_Globals.COST_SOFT;
					tVBalance.Text = GetText(Resource.String.balance) + TreasureHunt_Globals.GoldCurrency;
            });
        }
   
        protected override void OnDestroy()
        {
            base.OnDestroy();
            if (relativeLayoutMain != null)
            {
                relativeLayoutMain.SetBackgroundResource(0);
                relativeLayoutMain = null;
            }
            if (linearLayoutHolder != null)
            {
                linearLayoutHolder = null;
            }
            if (grid != null)
            {
                grid = null;
            }
            if (tvCost != null)
            {
                tvCost = null;
            }
            if (tVBalance != null)
            {
                tVBalance = null;
            }
            if (gridController != null)
            {
                gridController.DestroyMe();
                gridController = null;
            }
        }
		public override void OnBackPressed ()
		{
			BackToMaimMenu ();	
		}
        void btnMenuClick(object sender, EventArgs e)
        {
			BackToMaimMenu ();
        }

        void bntUnlockClick(object sender, EventArgs e)
        {

        }

		void BackToMaimMenu(){
			NotifyMe ();
			StartActivity(typeof(StarterActivity));
			Finish();
			OverridePendingTransition(Resource.Animation.SlideIn, Resource.Animation.slideOut);
		}

		void NotifyMe(){
            int index = rnd.Next(0, 3);
            string[] notifyMessageArray = new string[]{
				GetText(Resource.String.chancetowin25gold),
				GetText(Resource.String.chancetowin10gold),
				GetText(Resource.String.tryharder)
			};
            Notification.Builder notifBuilder = new Notification.Builder(this)
                                    .SetSmallIcon(Resource.Drawable.treasure_box_gold_coin_13)
				.SetContentTitle(GetText(Resource.String.treasurehunt))
                                    .SetContentText(String.Format(notifyMessageArray[index]));
            var notifManager = (NotificationManager)GetSystemService(Context.NotificationService);
			Intent myIntent = new Intent (this, typeof(StarterActivity));
			PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0,   myIntent, PendingIntentFlags.CancelCurrent);
            notifManager.Notify(2, notifBuilder.Build());

		}
    }
}

