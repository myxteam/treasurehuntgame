using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace TresureHunt.ScreenCode
{
    [Activity(Label = "BaseTabActivity")]
    public class BaseTabActivity : TabActivity
    {
        protected int layoutId = 0;
        LayoutManager layoutManager;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Create your application here

            var inch = TreasureHunt_Globals.storage.Get<string>(GetText(Resource.String.Inch));
            if (inch == null || inch == "")
            {
                DisplayMetrics dm = new DisplayMetrics();
                WindowManager.DefaultDisplay.GetMetrics(dm);
                int widthPixels = dm.WidthPixels;
                int heightPixels = dm.HeightPixels;
                double x = Math.Pow(widthPixels / dm.Xdpi, 2);
                double y = Math.Pow(heightPixels / dm.Ydpi, 2);
                double screenInches = Math.Round(Math.Sqrt(x + y));
                TreasureHunt_Globals.storage.Put(GetText(Resource.String.Inch), screenInches + "");
                TreasureHunt_Globals.inch = Convert.ToInt32(screenInches);
            }
            else
            {
                TreasureHunt_Globals.inch = Convert.ToInt32(inch);
            }
            layoutId = layoutManager.getLayout(this);
        }
    }
}