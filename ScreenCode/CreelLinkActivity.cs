using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using TreasureGameModels;

namespace TresureHunt.ScreenCode
{
	[Activity(Label = "RegisterCreel", Theme = "@android:style/Theme.NoTitleBar.Fullscreen", ScreenOrientation = ScreenOrientation.SensorLandscape)]
	public class CreelLinkActivity : BaseActivity
    {
		LinearLayout rootLinearLayout;
		Button btnNext, btnBack;
		EditText eTCreelID;
        protected override void OnCreate(Bundle bundle)
        {
            try
            {
                base.OnCreate(bundle);
                SetContentView(layoutId);
                // Create your application here
                rootLinearLayout = FindViewById<LinearLayout>
                    (Resource.Id.rootLinearLayout);
                btnNext = FindViewById<Button>(Resource.Id.btnNext);
                btnBack = FindViewById<Button>(Resource.Id.btnBack);
                eTCreelID = FindViewById<EditText>(Resource.Id.eTCreelID);

                btnBack.Click += btnBack_Click;
                btnNext.Click += btnNext_Click;
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
        }

      
		protected override void OnStart ()
		{
            try
            {
                base.OnStart();
                rootLinearLayout.SetBackgroundResource(Resource.Drawable.home_bg2);
                btnNext.SetBackgroundResource(Resource.Drawable.THPirateButton);
                btnBack.SetBackgroundResource(Resource.Drawable.THPirateButton);
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
		}
		protected override void OnDestroy ()
		{
			base.OnDestroy ();
			DisposeWidget (btnNext);
			DisposeWidget (btnBack);
			DisposeWidgetEditText (eTCreelID);
			if (rootLinearLayout != null) {
				rootLinearLayout.SetBackgroundResource (0);
				rootLinearLayout = null;
			}

		}
		void btnBack_Click(object sender, EventArgs e)
		{			
			Finish ();
		}
		async void btnNext_Click(object sender, EventArgs e)
		{
            try
            {
                String device = Build.Model;
				string OS = GetText(Resource.String.osmodel) + Build.VERSION.Sdk;
                string manufacturer = Build.Manufacturer;
				var x = await ApiCallsExtendtion.Instance.LoginCreel(new LogInModel { CreelUserName = eTCreelID.Text, DeviceName = device, Manufacturer = manufacturer, Model = OS, UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid), DeviceId = TreasureHunt_Globals.DeviceId }, true, this);

                if (x.Data == null)
                {
					var answer = await ShowYesOrNoDialog(x.ErrorString, GetText(Resource.String.signUpWithCreel), GetText(Resource.String.close));
                    if (answer == true)
                    {
                        String url = x.infoString.ToString();
                        Intent i = new Intent(Intent.ActionView);
                        i.SetData(Android.Net.Uri.Parse(url));
                        StartActivity(i);
                        this.Finish();
                    }
                }
                else
                {
					TreasureHunt_Globals.CreelUserName = eTCreelID.Text;
                    TreasureHunt_Globals.Customerid = Guid.Parse(x.Data.ToString());
                    TreasureHunt_Globals.getCreel = true;
					await ShowOKDialog(GetText(Resource.String.linkSucessful));
                    this.Finish();
                }
            }
            catch (Exception ex)
            {
                ApiCallsExtendtion.Instance.Error_Send(ex);
            }
		}
    }
}		