#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

[assembly: global::Android.Runtime.ResourceDesignerAttribute("TresureHunt.Resource", IsApplication=true)]

namespace TresureHunt
{
	
	
	[System.CodeDom.Compiler.GeneratedCodeAttribute("Xamarin.Android.Build.Tasks", "1.0.0.0")]
	public partial class Resource
	{
		
		static Resource()
		{
			global::Android.Runtime.ResourceIdManager.UpdateIdValues();
		}
		
		public static void UpdateIdValues()
		{
			global::PerpetualMobile.Storage.Resource.String.library_name = global::TresureHunt.Resource.String.library_name;
			global::TreasureHuntLib.Resource.String.ApplicationName = global::TresureHunt.Resource.String.ApplicationName;
			global::TreasureHuntLib.Resource.String.Hello = global::TresureHunt.Resource.String.Hello;
		}
		
		public partial class Animation
		{
			
			// aapt resource value: 0x7f040000
			public const int animate_treasure_empty = 2130968576;
			
			// aapt resource value: 0x7f040001
			public const int animate_treasure_gold_one = 2130968577;
			
			// aapt resource value: 0x7f040002
			public const int SlideIn = 2130968578;
			
			// aapt resource value: 0x7f040003
			public const int SlideInBack = 2130968579;
			
			// aapt resource value: 0x7f040004
			public const int slideOut = 2130968580;
			
			// aapt resource value: 0x7f040005
			public const int slideOutBack = 2130968581;
			
			static Animation()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Animation()
			{
			}
		}
		
		public partial class Attribute
		{
			
			static Attribute()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Attribute()
			{
			}
		}
		
		public partial class Color
		{
			
			// aapt resource value: 0x7f070002
			public const int back_button_green = 2131165186;
			
			// aapt resource value: 0x7f070003
			public const int black = 2131165187;
			
			// aapt resource value: 0x7f070005
			public const int brown = 2131165189;
			
			// aapt resource value: 0x7f070001
			public const int cyan = 2131165185;
			
			// aapt resource value: 0x7f070004
			public const int gold = 2131165188;
			
			// aapt resource value: 0x7f070006
			public const int timezapp_background = 2131165190;
			
			// aapt resource value: 0x7f070017
			public const int timezapp_border_color = 2131165207;
			
			// aapt resource value: 0x7f07000b
			public const int timezapp_darker_background = 2131165195;
			
			// aapt resource value: 0x7f070013
			public const int timezapp_diamond_background = 2131165203;
			
			// aapt resource value: 0x7f070014
			public const int timezapp_diamond_text_color = 2131165204;
			
			// aapt resource value: 0x7f070015
			public const int timezapp_diamond_title_color = 2131165205;
			
			// aapt resource value: 0x7f07000e
			public const int timezapp_disabled_background = 2131165198;
			
			// aapt resource value: 0x7f070007
			public const int timezapp_green_background = 2131165191;
			
			// aapt resource value: 0x7f07000a
			public const int timezapp_letthis_white_font = 2131165194;
			
			// aapt resource value: 0x7f070009
			public const int timezapp_login_background = 2131165193;
			
			// aapt resource value: 0x7f070008
			public const int timezapp_loginandstartnow_background = 2131165192;
			
			// aapt resource value: 0x7f07000c
			public const int timezapp_question_header_background = 2131165196;
			
			// aapt resource value: 0x7f07000d
			public const int timezapp_question_text_font = 2131165197;
			
			// aapt resource value: 0x7f070011
			public const int timezapp_register_button_default = 2131165201;
			
			// aapt resource value: 0x7f070012
			public const int timezapp_register_button_pressed = 2131165202;
			
			// aapt resource value: 0x7f070016
			public const int timezapp_register_intro_background = 2131165206;
			
			// aapt resource value: 0x7f07000f
			public const int timezapp_rewards_green_background = 2131165199;
			
			// aapt resource value: 0x7f070010
			public const int timezapp_rewards_green_darker_background = 2131165200;
			
			// aapt resource value: 0x7f070000
			public const int white = 2131165184;
			
			static Color()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Color()
			{
			}
		}
		
		public partial class Drawable
		{
			
			// aapt resource value: 0x7f020000
			public const int bg_treasure_background = 2130837504;
			
			// aapt resource value: 0x7f020001
			public const int btn_back_close = 2130837505;
			
			// aapt resource value: 0x7f020002
			public const int btn_back_open = 2130837506;
			
			// aapt resource value: 0x7f020003
			public const int chestcave_bg = 2130837507;
			
			// aapt resource value: 0x7f020004
			public const int chestcave_gold = 2130837508;
			
			// aapt resource value: 0x7f020005
			public const int chestcave_scroll = 2130837509;
			
			// aapt resource value: 0x7f020006
			public const int CompleteFlagButton = 2130837510;
			
			// aapt resource value: 0x7f020007
			public const int contactserver_name = 2130837511;
			
			// aapt resource value: 0x7f020008
			public const int contactserver_plate = 2130837512;
			
			// aapt resource value: 0x7f020009
			public const int contactserverlogo = 2130837513;
			
			// aapt resource value: 0x7f02000a
			public const int creel_btn = 2130837514;
			
			// aapt resource value: 0x7f02000b
			public const int creel_btn2_close2 = 2130837515;
			
			// aapt resource value: 0x7f02000c
			public const int creel_btn2_open2 = 2130837516;
			
			// aapt resource value: 0x7f02000d
			public const int creel_btn_close = 2130837517;
			
			// aapt resource value: 0x7f02000e
			public const int CreelButton = 2130837518;
			
			// aapt resource value: 0x7f02000f
			public const int creelgold_btn = 2130837519;
			
			// aapt resource value: 0x7f020010
			public const int creelgold_btn_click = 2130837520;
			
			// aapt resource value: 0x7f020011
			public const int entercreel_panel = 2130837521;
			
			// aapt resource value: 0x7f020012
			public const int game_btn = 2130837522;
			
			// aapt resource value: 0x7f020013
			public const int game_btn2_close2 = 2130837523;
			
			// aapt resource value: 0x7f020014
			public const int game_btn2_open2 = 2130837524;
			
			// aapt resource value: 0x7f020015
			public const int game_btn_close = 2130837525;
			
			// aapt resource value: 0x7f020016
			public const int gold_coin = 2130837526;
			
			// aapt resource value: 0x7f020017
			public const int Gold_Gradient = 2130837527;
			
			// aapt resource value: 0x7f020018
			public const int home_bg = 2130837528;
			
			// aapt resource value: 0x7f020019
			public const int home_bg2 = 2130837529;
			
			// aapt resource value: 0x7f02001a
			public const int ic_refresh = 2130837530;
			
			// aapt resource value: 0x7f02001b
			public const int ic_refresh_hover = 2130837531;
			
			// aapt resource value: 0x7f02001c
			public const int Icon = 2130837532;
			
			// aapt resource value: 0x7f02001d
			public const int IcRefreshClick = 2130837533;
			
			// aapt resource value: 0x7f02001e
			public const int LeftFlagButton = 2130837534;
			
			// aapt resource value: 0x7f02001f
			public const int LineBlueGradient = 2130837535;
			
			// aapt resource value: 0x7f020020
			public const int portfolio_bg = 2130837536;
			
			// aapt resource value: 0x7f020021
			public const int portfolio_popup_creel = 2130837537;
			
			// aapt resource value: 0x7f020022
			public const int portfolio_popup_panel = 2130837538;
			
			// aapt resource value: 0x7f020023
			public const int profile_pic = 2130837539;
			
			// aapt resource value: 0x7f020024
			public const int RightFlagButton = 2130837540;
			
			// aapt resource value: 0x7f020025
			public const int thICON2_60 = 2130837541;
			
			// aapt resource value: 0x7f020026
			public const int THPirateButton = 2130837542;
			
			// aapt resource value: 0x7f020027
			public const int treasure_box_empty_1 = 2130837543;
			
			// aapt resource value: 0x7f020028
			public const int treasure_box_empty_2 = 2130837544;
			
			// aapt resource value: 0x7f020029
			public const int treasure_box_empty_3 = 2130837545;
			
			// aapt resource value: 0x7f02002a
			public const int treasure_box_empty_4 = 2130837546;
			
			// aapt resource value: 0x7f02002b
			public const int treasure_box_empty_5 = 2130837547;
			
			// aapt resource value: 0x7f02002c
			public const int treasure_box_gold_coin_11 = 2130837548;
			
			// aapt resource value: 0x7f02002d
			public const int treasure_box_gold_coin_12 = 2130837549;
			
			// aapt resource value: 0x7f02002e
			public const int treasure_box_gold_coin_13 = 2130837550;
			
			// aapt resource value: 0x7f02002f
			public const int treasure_box_gold_coin_14 = 2130837551;
			
			// aapt resource value: 0x7f020030
			public const int treasure_box_gold_coin_15 = 2130837552;
			
			// aapt resource value: 0x7f020031
			public const int treasure_btn = 2130837553;
			
			// aapt resource value: 0x7f020032
			public const int treasure_btn_click = 2130837554;
			
			// aapt resource value: 0x7f020033
			public const int treasure_logo2 = 2130837555;
			
			// aapt resource value: 0x7f020034
			public const int Wallet_bg = 2130837556;
			
			// aapt resource value: 0x7f020035
			public const int welcome_panel = 2130837557;
			
			static Drawable()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Drawable()
			{
			}
		}
		
		public partial class Id
		{
			
			// aapt resource value: 0x7f08002e
			public const int LinearLayoutHolder = 2131230766;
			
			// aapt resource value: 0x7f080012
			public const int TVAmm = 2131230738;
			
			// aapt resource value: 0x7f080020
			public const int TVCreelAmount = 2131230752;
			
			// aapt resource value: 0x7f08001f
			public const int TVCreelAmountAvailable = 2131230751;
			
			// aapt resource value: 0x7f080018
			public const int TVGame = 2131230744;
			
			// aapt resource value: 0x7f08001d
			public const int TVWallet = 2131230749;
			
			// aapt resource value: 0x7f080022
			public const int aCUserName = 2131230754;
			
			// aapt resource value: 0x7f08000d
			public const int aboutCreelbtn = 2131230733;
			
			// aapt resource value: 0x7f080004
			public const int btnBack = 2131230724;
			
			// aapt resource value: 0x7f080009
			public const int btnCreelBack = 2131230729;
			
			// aapt resource value: 0x7f080011
			public const int btnDecAmount = 2131230737;
			
			// aapt resource value: 0x7f080026
			public const int btnDedication = 2131230758;
			
			// aapt resource value: 0x7f080013
			public const int btnIncAmount = 2131230739;
			
			// aapt resource value: 0x7f080019
			public const int btnLeft = 2131230745;
			
			// aapt resource value: 0x7f080039
			public const int btnLogin = 2131230777;
			
			// aapt resource value: 0x7f08002c
			public const int btnMenu = 2131230764;
			
			// aapt resource value: 0x7f08002d
			public const int btnNewGame = 2131230765;
			
			// aapt resource value: 0x7f080005
			public const int btnNext = 2131230725;
			
			// aapt resource value: 0x7f08003e
			public const int btnNo = 2131230782;
			
			// aapt resource value: 0x7f080029
			public const int btnOk = 2131230761;
			
			// aapt resource value: 0x7f080038
			public const int btnPorfolio = 2131230776;
			
			// aapt resource value: 0x7f08003a
			public const int btnQuit = 2131230778;
			
			// aapt resource value: 0x7f08001e
			public const int btnRight = 2131230750;
			
			// aapt resource value: 0x7f080025
			public const int btnSend = 2131230757;
			
			// aapt resource value: 0x7f080037
			public const int btnStart = 2131230775;
			
			// aapt resource value: 0x7f08003d
			public const int btnYes = 2131230781;
			
			// aapt resource value: 0x7f080036
			public const int creelBtn = 2131230774;
			
			// aapt resource value: 0x7f080024
			public const int eTAmount = 2131230756;
			
			// aapt resource value: 0x7f080002
			public const int eTCreelID = 2131230722;
			
			// aapt resource value: 0x7f080028
			public const int eTDedicationMessage = 2131230760;
			
			// aapt resource value: 0x7f080003
			public const int frameLayout1 = 2131230723;
			
			// aapt resource value: 0x7f080001
			public const int imageView1 = 2131230721;
			
			// aapt resource value: 0x7f080031
			public const int imageView3 = 2131230769;
			
			// aapt resource value: 0x7f080033
			public const int imageView4 = 2131230771;
			
			// aapt resource value: 0x7f080030
			public const int imageView5 = 2131230768;
			
			// aapt resource value: 0x7f080027
			public const int lLMessageHolder = 2131230759;
			
			// aapt resource value: 0x7f080008
			public const int linearLayout1 = 2131230728;
			
			// aapt resource value: 0x7f080014
			public const int linearLayout2 = 2131230740;
			
			// aapt resource value: 0x7f080015
			public const int linearLayout3 = 2131230741;
			
			// aapt resource value: 0x7f08001a
			public const int linearLayout4 = 2131230746;
			
			// aapt resource value: 0x7f080010
			public const int linearLayout5 = 2131230736;
			
			// aapt resource value: 0x7f08000a
			public const int linkCreelbtn = 2131230730;
			
			// aapt resource value: 0x7f08002a
			public const int progressBar1 = 2131230762;
			
			// aapt resource value: 0x7f08000e
			public const int relativeLayout1 = 2131230734;
			
			// aapt resource value: 0x7f08002b
			public const int relativeLayoutMain = 2131230763;
			
			// aapt resource value: 0x7f080007
			public const int rootLInearLayout = 2131230727;
			
			// aapt resource value: 0x7f080000
			public const int rootLinearLayout = 2131230720;
			
			// aapt resource value: 0x7f08000c
			public const int sendCreelbtn = 2131230732;
			
			// aapt resource value: 0x7f080023
			public const int tVAmount = 2131230755;
			
			// aapt resource value: 0x7f080034
			public const int tVBalance = 2131230772;
			
			// aapt resource value: 0x7f08002f
			public const int tVCost = 2131230767;
			
			// aapt resource value: 0x7f08001b
			public const int tVCreelAccount = 2131230747;
			
			// aapt resource value: 0x7f080016
			public const int tVGameAccount = 2131230742;
			
			// aapt resource value: 0x7f080032
			public const int tVGoldBalance = 2131230770;
			
			// aapt resource value: 0x7f08003b
			public const int tVNotice = 2131230779;
			
			// aapt resource value: 0x7f08001c
			public const int tVSpendingCapacity = 2131230748;
			
			// aapt resource value: 0x7f080017
			public const int tVSpendingCapacity2 = 2131230743;
			
			// aapt resource value: 0x7f08000f
			public const int tVTransferableAmount = 2131230735;
			
			// aapt resource value: 0x7f080021
			public const int tVUserName = 2131230753;
			
			// aapt resource value: 0x7f080006
			public const int textView1 = 2131230726;
			
			// aapt resource value: 0x7f080035
			public const int thirdPatyLayout = 2131230773;
			
			// aapt resource value: 0x7f08000b
			public const int transferCreelbtn = 2131230731;
			
			// aapt resource value: 0x7f08003c
			public const int txtMessage = 2131230780;
			
			static Id()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Id()
			{
			}
		}
		
		public partial class Layout
		{
			
			// aapt resource value: 0x7f030000
			public const int CreelLink = 2130903040;
			
			// aapt resource value: 0x7f030001
			public const int CreelMenu = 2130903041;
			
			// aapt resource value: 0x7f030002
			public const int CreelTransfer = 2130903042;
			
			// aapt resource value: 0x7f030003
			public const int CreelTransfer_Tablet = 2130903043;
			
			// aapt resource value: 0x7f030004
			public const int CreelTransfer_Tablet_Ten = 2130903044;
			
			// aapt resource value: 0x7f030005
			public const int CreelUserTransfer = 2130903045;
			
			// aapt resource value: 0x7f030006
			public const int CreelUserTransfer_Tablet = 2130903046;
			
			// aapt resource value: 0x7f030007
			public const int CreelUserTransfer_Tablet_Ten = 2130903047;
			
			// aapt resource value: 0x7f030008
			public const int LoadingMessage = 2130903048;
			
			// aapt resource value: 0x7f030009
			public const int MainGame = 2130903049;
			
			// aapt resource value: 0x7f03000a
			public const int StartPage = 2130903050;
			
			// aapt resource value: 0x7f03000b
			public const int StartPage_Tablet = 2130903051;
			
			// aapt resource value: 0x7f03000c
			public const int StartPage_Tablet_Ten = 2130903052;
			
			// aapt resource value: 0x7f03000d
			public const int WinNoticeDialog = 2130903053;
			
			// aapt resource value: 0x7f03000e
			public const int YesNoDialog = 2130903054;
			
			static Layout()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Layout()
			{
			}
		}
		
		public partial class Raw
		{
			
			// aapt resource value: 0x7f050000
			public const int theme2 = 2131034112;
			
			static Raw()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private Raw()
			{
			}
		}
		
		public partial class String
		{
			
			// aapt resource value: 0x7f060001
			public const int ApplicationName = 2131099649;
			
			// aapt resource value: 0x7f060000
			public const int Hello = 2131099648;
			
			// aapt resource value: 0x7f06004b
			public const int about = 2131099723;
			
			// aapt resource value: 0x7f060024
			public const int amount = 2131099684;
			
			// aapt resource value: 0x7f060025
			public const int amountGold = 2131099685;
			
			// aapt resource value: 0x7f060027
			public const int amounts = 2131099687;
			
			// aapt resource value: 0x7f060058
			public const int amounttransferred = 2131099736;
			
			// aapt resource value: 0x7f060003
			public const int applicationName = 2131099651;
			
			// aapt resource value: 0x7f060055
			public const int arr = 2131099733;
			
			// aapt resource value: 0x7f060035
			public const int balance = 2131099701;
			
			// aapt resource value: 0x7f06001e
			public const int buy = 2131099678;
			
			// aapt resource value: 0x7f060033
			public const int buyNow = 2131099699;
			
			// aapt resource value: 0x7f06005c
			public const int cancel = 2131099740;
			
			// aapt resource value: 0x7f06004c
			public const int cannotContactServerRetry = 2131099724;
			
			// aapt resource value: 0x7f06002d
			public const int cannotReachServer = 2131099693;
			
			// aapt resource value: 0x7f06006b
			public const int chancetowin10gold = 2131099755;
			
			// aapt resource value: 0x7f06006a
			public const int chancetowin25gold = 2131099754;
			
			// aapt resource value: 0x7f060026
			public const int changeCurrencies = 2131099686;
			
			// aapt resource value: 0x7f060022
			public const int charging = 2131099682;
			
			// aapt resource value: 0x7f060066
			public const int chestalreadyopen = 2131099750;
			
			// aapt resource value: 0x7f060051
			public const int close = 2131099729;
			
			// aapt resource value: 0x7f060067
			public const int comingsoon = 2131099751;
			
			// aapt resource value: 0x7f06004d
			public const int contactServerDialog = 2131099725;
			
			// aapt resource value: 0x7f06002b
			public const int contactingServer = 2131099691;
			
			// aapt resource value: 0x7f060014
			public const int copper = 2131099668;
			
			// aapt resource value: 0x7f060034
			public const int costToOpen = 2131099700;
			
			// aapt resource value: 0x7f06000d
			public const int createAccount = 2131099661;
			
			// aapt resource value: 0x7f060057
			public const int creel = 2131099735;
			
			// aapt resource value: 0x7f060029
			public const int creelUserName = 2131099689;
			
			// aapt resource value: 0x7f060059
			public const int creelaccount = 2131099737;
			
			// aapt resource value: 0x7f060046
			public const int creelgold = 2131099718;
			
			// aapt resource value: 0x7f060060
			public const int creelmenu = 2131099744;
			
			// aapt resource value: 0x7f060069
			public const int creelsandbox = 2131099753;
			
			// aapt resource value: 0x7f06005d
			public const int creelserverdown = 2131099741;
			
			// aapt resource value: 0x7f060023
			public const int currency = 2131099683;
			
			// aapt resource value: 0x7f060048
			public const int dedication = 2131099720;
			
			// aapt resource value: 0x7f060068
			public const int dialogfragment = 2131099752;
			
			// aapt resource value: 0x7f060012
			public const int diamond = 2131099666;
			
			// aapt resource value: 0x7f06003d
			public const int dontHaveEnoughGold = 2131099709;
			
			// aapt resource value: 0x7f06003e
			public const int dontHaveEnoughGold2 = 2131099710;
			
			// aapt resource value: 0x7f060044
			public const int donthavecreel = 2131099716;
			
			// aapt resource value: 0x7f060013
			public const int doubloon = 2131099667;
			
			// aapt resource value: 0x7f06004a
			public const int erroroccured = 2131099722;
			
			// aapt resource value: 0x7f06000c
			public const int exchange = 2131099660;
			
			// aapt resource value: 0x7f060016
			public const int exit = 2131099670;
			
			// aapt resource value: 0x7f060045
			public const int gameaccount = 2131099717;
			
			// aapt resource value: 0x7f06005a
			public const int getfromcreel = 2131099738;
			
			// aapt resource value: 0x7f06000f
			public const int gold = 2131099663;
			
			// aapt resource value: 0x7f06005b
			public const int googleplay = 2131099739;
			
			// aapt resource value: 0x7f060037
			public const int guess = 2131099703;
			
			// aapt resource value: 0x7f060039
			public const int inch = 2131099705;
			
			// aapt resource value: 0x7f060038
			public const int info = 2131099704;
			
			// aapt resource value: 0x7f060031
			public const int insufficient = 2131099697;
			
			// aapt resource value: 0x7f060032
			public const int insufficientbalance = 2131099698;
			
			// aapt resource value: 0x7f06002e
			public const int invalidUsernamePassword = 2131099694;
			
			// aapt resource value: 0x7f060002
			public const int library_name = 2131099650;
			
			// aapt resource value: 0x7f060040
			public const int linkSucessful = 2131099712;
			
			// aapt resource value: 0x7f060041
			public const int linkcreel = 2131099713;
			
			// aapt resource value: 0x7f060015
			public const int loading = 2131099669;
			
			// aapt resource value: 0x7f060009
			public const int login = 2131099657;
			
			// aapt resource value: 0x7f06000a
			public const int logout = 2131099658;
			
			// aapt resource value: 0x7f060054
			public const int mariana = 2131099732;
			
			// aapt resource value: 0x7f06001c
			public const int menu = 2131099676;
			
			// aapt resource value: 0x7f06002c
			public const int networkError = 2131099692;
			
			// aapt resource value: 0x7f060006
			public const int next = 2131099654;
			
			// aapt resource value: 0x7f06004f
			public const int no = 2131099727;
			
			// aapt resource value: 0x7f060064
			public const int notenoughgold = 2131099748;
			
			// aapt resource value: 0x7f06001d
			public const int noticMsg = 2131099677;
			
			// aapt resource value: 0x7f060050
			public const int ok = 2131099728;
			
			// aapt resource value: 0x7f060056
			public const int osmodel = 2131099734;
			
			// aapt resource value: 0x7f060053
			public const int parchment = 2131099731;
			
			// aapt resource value: 0x7f060005
			public const int password = 2131099653;
			
			// aapt resource value: 0x7f060011
			public const int pearl = 2131099665;
			
			// aapt resource value: 0x7f06003c
			public const int pleaseFillOutForm = 2131099708;
			
			// aapt resource value: 0x7f06002f
			public const int pleaseLoginFirst = 2131099695;
			
			// aapt resource value: 0x7f060019
			public const int portfolio = 2131099673;
			
			// aapt resource value: 0x7f060007
			public const int previous = 2131099655;
			
			// aapt resource value: 0x7f06000b
			public const int register = 2131099659;
			
			// aapt resource value: 0x7f060017
			public const int registerCreel = 2131099671;
			
			// aapt resource value: 0x7f06003f
			public const int registerSucessful = 2131099711;
			
			// aapt resource value: 0x7f060030
			public const int requiresInternetConnection = 2131099696;
			
			// aapt resource value: 0x7f060021
			public const int send = 2131099681;
			
			// aapt resource value: 0x7f060020
			public const int sendCreel = 2131099680;
			
			// aapt resource value: 0x7f060043
			public const int sendgold = 2131099715;
			
			// aapt resource value: 0x7f060052
			public const int signUpWithCreel = 2131099730;
			
			// aapt resource value: 0x7f060010
			public const int silver = 2131099664;
			
			// aapt resource value: 0x7f060065
			public const int sorrytryagain = 2131099749;
			
			// aapt resource value: 0x7f060047
			public const int spendingcapacity = 2131099719;
			
			// aapt resource value: 0x7f06000e
			public const int start = 2131099662;
			
			// aapt resource value: 0x7f06001a
			public const int statement_1 = 2131099674;
			
			// aapt resource value: 0x7f06005e
			public const int transactionfailed = 2131099742;
			
			// aapt resource value: 0x7f06001f
			public const int transfer = 2131099679;
			
			// aapt resource value: 0x7f06003a
			public const int transferSuccessful = 2131099706;
			
			// aapt resource value: 0x7f060028
			public const int transferToUser = 2131099688;
			
			// aapt resource value: 0x7f06002a
			public const int transferableAmount = 2131099690;
			
			// aapt resource value: 0x7f060042
			public const int transfercreelgold = 2131099714;
			
			// aapt resource value: 0x7f06003b
			public const int transfersuccess = 2131099707;
			
			// aapt resource value: 0x7f060061
			public const int treasurehunt = 2131099745;
			
			// aapt resource value: 0x7f06006c
			public const int tryharder = 2131099756;
			
			// aapt resource value: 0x7f060063
			public const int unlinkcantcomplete = 2131099747;
			
			// aapt resource value: 0x7f060062
			public const int unlinkcreel = 2131099746;
			
			// aapt resource value: 0x7f06001b
			public const int unlock = 2131099675;
			
			// aapt resource value: 0x7f060004
			public const int userName = 2131099652;
			
			// aapt resource value: 0x7f06005f
			public const int usernotfound = 2131099743;
			
			// aapt resource value: 0x7f060008
			public const int wallet = 2131099656;
			
			// aapt resource value: 0x7f060036
			public const int welcome = 2131099702;
			
			// aapt resource value: 0x7f060049
			public const int welcomeguest = 2131099721;
			
			// aapt resource value: 0x7f06004e
			public const int yes = 2131099726;
			
			// aapt resource value: 0x7f060018
			public const int yourNumber = 2131099672;
			
			static String()
			{
				global::Android.Runtime.ResourceIdManager.UpdateIdValues();
			}
			
			private String()
			{
			}
		}
	}
}
#pragma warning restore 1591
