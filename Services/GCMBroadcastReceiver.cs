﻿using System;
using Android.Content;
using Android.App;
using tresureHunt.tresureHunt;

namespace tH.tresureHunt
{
	[BroadcastReceiver(Permission= "com.google.android.c2dm.permission.SEND")]
	[IntentFilter(new string[] { "com.google.android.c2dm.intent.RECEIVE" }, Categories = new string[] {"tresureHunt.tresureHunt" },Priority = (int)IntentFilterPriority.HighPriority)]
	[IntentFilter(new string[] { "com.google.android.c2dm.intent.REGISTRATION" }, Categories = new string[] {"tresureHunt.tresureHunt" })]
	[IntentFilter(new string[] { "com.google.android.gcm.intent.RETRY" }, Categories = new string[] { "tresureHunt.tresureHunt"})]
	[IntentFilter(new[] { Android.Content.Intent.ActionBootCompleted })]
	public class GCMBroadcastReceiver : BroadcastReceiver
	{
		const string TAG = "PushHandlerBroadcastReceiver";
		public override void OnReceive(Context context, Intent intent)
		{
			ComponentName comp = new ComponentName(context.PackageName,
				typeof(PushNotificationIntentService).Name);

			PushNotificationIntentService.RunIntentInService(context, intent.SetComponent(comp));
				
			SetResult(Result.Ok, null, null);
		}
	}
}

