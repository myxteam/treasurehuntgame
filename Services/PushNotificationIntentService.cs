﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TresureHunt;

namespace tresureHunt.tresureHunt
{
	[Service]			
	public class PushNotificationIntentService : Android.App.IntentService
	{
		static PowerManager.WakeLock wakeLock;
		static object LOCK = new object();

		public static void RunIntentInService(Context context, Intent intent)
		{
			lock (LOCK)
			{
				if (wakeLock == null)
				{
					var pm = PowerManager.FromContext(context);
					wakeLock = pm.NewWakeLock(WakeLockFlags.Partial, "pushnotifLock");
				}
			}

			wakeLock.Acquire();
			intent.SetClass(context, typeof(PushNotificationIntentService));
			context.StartService(intent);
		}

		protected override void OnHandleIntent (Intent intent)
		{
			try
			{
				Context context = this.ApplicationContext;
				string action = intent.Action;

				if (action.Equals("com.google.android.c2dm.intent.REGISTRATION"))
				{
					string registrationId = intent.GetStringExtra("registration_id");

					if (string.IsNullOrEmpty(registrationId) || registrationId == ""){
						string senders = "135366940210";

						Intent _intent = new Intent("com.google.android.c2dm.intent.REGISTER");
						_intent.SetPackage("com.google.android.gsf");
						_intent.PutExtra("app", PendingIntent.GetBroadcast(this, 0, new Intent(), 0));
						_intent.PutExtra("sender", senders);
						this.StartService(_intent);
					}
					TreasureHunt_Globals.registrationId = registrationId;
				}
				else if (action.Equals("com.google.android.c2dm.intent.RECEIVE"))
				{
					var notifManager = (NotificationManager)context.GetSystemService (Context.NotificationService);
					var notification = new Notification (Resource.Drawable.Icon, "Treasure Hunt");
					var pendingIntent = PendingIntent.GetActivity (context, 0, new Intent (context, typeof(StarterActivity)), PendingIntentFlags.UpdateCurrent);

					Notification.Builder notifBuilder = new Notification.Builder(context)
                        .SetSmallIcon(Resource.Drawable.treasure_box_gold_coin_13)
						.SetContentTitle("Treasure Hunt")
						.SetContentIntent(pendingIntent)
						.SetContentText(intent.GetStringExtra("msg"))
						.SetStyle(new Notification.BigTextStyle().BigText(intent.GetStringExtra("msg")));

					notifManager.Notify (TreasureHunt_Globals.uniqueId, notifBuilder.Build());
					TreasureHunt_Globals.uniqueId++;

				}
			}
			finally
			{
				lock (LOCK)
				{
					//Sanity check for null as this is a public method
					if (wakeLock != null)
						wakeLock.Release();
				}
			}
		}

		private void RegisterDevice ()
		{
			
		}
	}
}

