using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using TresureHunt;
using System.Threading.Tasks;
using TreasureGameModels;

namespace TresureHunt
{
	public class GameController
	{
		public static TreasureGame.TreasureGame CurrentGame { get; set; }

		public static async Task<Boolean> NewPirateTreasureGame(int collums, int rows, bool show, BaseActivity act)
		{
			
			LogInModel model = new LogInModel();
			try{
			model.GameID = TreasureHunt_Globals.GameId;
			model.Password = TreasureHunt_Globals.Password;
			model.UserName = TreasureHunt_Globals.Username;
			model.UserGuid = Guid.Parse(TreasureHunt_Globals.Userguid);

			var x = await ApiCallsExtendtion.Instance.GetNewGame(model, show,act);
			var y = x.Data.ToString().Split(':');
			TreasureHunt_Globals.CurrentSession = int.Parse(y[0]);
			TreasureGame.TreasureGame newGame = new TreasureGame.TreasureGame(int.Parse(y[0]), int.Parse(y[2]), int.Parse(y[2]), x.Data.ToString());
			GameController.CurrentGame = newGame;
			HasGame = true;
			return true;
			}catch(Exception ex){
				ApiCallsExtendtion.Instance.Error_Send (ex);
				return false;
			}
		}

		public static bool HasGame;

	}
}