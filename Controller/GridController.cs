﻿using System;
using TreasureGame;
using Android.Widget;
using Android.App;
using Android.Content;
using Android.Graphics.Drawables;
using System.Threading.Tasks;
using DI.PoorMansContainer;

namespace TresureHunt
{

	public class WinArgs : EventArgs
	{
		public int ammont;
		public TreasureHuntLib.TreasureContains treasureContains;
	}

	public class GridController
	{
		public delegate void ChangedEventHandler (object sender, EventArgs e);

		public event ChangedEventHandler Changed;

		public delegate void QueryEventHandler (object sender, EventArgs e);

		public event QueryEventHandler Query;

		public delegate void GameEventHandler (object sender, WinArgs e);

		public event GameEventHandler GameEvent;

		Grid grid;
		bool locking = false;
		Container container;

		public GridController (Grid grid)
		{
			this.grid = grid;
			container = new Container ();
		}

		protected virtual void OnChanged (EventArgs e)
		{
			if (Changed != null)
				Changed (this, e);
		}

		protected virtual void OnGameEvent (WinArgs e)
		{
			if (GameEvent != null)
				GameEvent (this, e);
		}

		protected virtual void OnQuery (EventArgs e)
		{
			if (Query != null)
				Query (this, e);
		}

		public void initGame (ref LinearLayout linearLayout, Context context, FragmentManager frag)
		{
			LinearLayout.LayoutParams param = new LinearLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent, 1f);
			param.SetMargins (10, 10, 10, 10);
			int collumnsCount = MarginValue (grid.Collums);
			collumnsCount = pxFromDp (collumnsCount, context);
			var par = new LinearLayout.LayoutParams (pxFromDp (80, context), pxFromDp (75, context));
			par.SetMargins (collumnsCount, 0, collumnsCount, 0);
			for (int x = 0; x < grid.Rows; x++) {
				LinearLayout linearLayoutextend = new LinearLayout (context);
				param.Gravity = Android.Views.GravityFlags.Center;
				linearLayoutextend.LayoutParameters = param;
				linearLayoutextend.SetGravity (Android.Views.GravityFlags.Center);
				for (int y = 0; y < grid.Collums; y++) {
					var customImageView = container.CreateType<CustomImageView> ();
					customImageView.ImageView = new ImageView (context);
					customImageView.ImageView.LayoutParameters = par;
					switch (grid.getChest (x, y).ChestType) {
					case ChestType.Closed_PirateChest_Empty:
						customImageView.ImageView.SetBackgroundResource (Resource.Animation.animate_treasure_empty);
						break;
					case ChestType.Closed_PirateChest_Gold:
						customImageView.ImageView.SetBackgroundResource (Resource.Animation.animate_treasure_gold_one);
						break;
					}
					customImageView.ImageView.Id = x;
					customImageView.GridX = x;
					customImageView.GridY = y;

					customImageView.ImageView.Click += async delegate(object sender, EventArgs e) {
						if (!locking) {
							if (grid.getChest (customImageView.GridX, customImageView.GridY).isOpen) {
								return;
							}

							int cost = TreasureHunt_Globals.COST_SOFT;

							if (!(cost <= TreasureHunt_Globals.GoldCurrency)) {
								OnQuery (EventArgs.Empty);
								return;
							}

							OnChanged (EventArgs.Empty);
							locking = true;
							AnimationDrawable animation = (AnimationDrawable)customImageView.ImageView.Background;
							animation.OneShot = true;
							animation.Start ();
							await Task.Delay (480);
							try{
							ShowDialog (frag, grid.getChest (customImageView.GridX, customImageView.GridY).ChestType, grid.getChest (customImageView.GridX, customImageView.GridY), grid.getChest (customImageView.GridX, customImageView.GridY).Ammount, false, "");
							}catch{
							}
							locking = false;
						}
					};
					linearLayoutextend.AddView (customImageView.ImageView);
				}
				linearLayout.AddView (linearLayoutextend);
			}
		}

		public void DestroyMe ()
		{
			if (grid != null)
				grid = null;
			if (container != null)
				container = null;
		}

		public async void ShowDialog (FragmentManager frag, ChestType chestType, Chest chest, int ammount, bool isText, String text)
		{
			chest.isOpen = true;
			WinArgs wa = new WinArgs ();

			switch (chestType) {
			case ChestType.Closed_PirateChest_Silver:					
				wa.ammont = ammount;
				wa.treasureContains = TreasureHuntLib.TreasureContains.SilverCoins;
				OnGameEvent (wa);
				break;
			case ChestType.Closed_PirateChest_Gold:		
				wa.ammont = ammount;
				wa.treasureContains = TreasureHuntLib.TreasureContains.GoldCoins;
				OnGameEvent (wa);
				break;
			case ChestType.Closed_PirateChest_Doubloon:
				wa.ammont = ammount;
				wa.treasureContains = TreasureHuntLib.TreasureContains.DubloonCoins;
				OnGameEvent (wa);
				break;
			case ChestType.Closed_PirateChest_Copper:
				wa.ammont = ammount;
				wa.treasureContains = TreasureHuntLib.TreasureContains.CopperCoins;
				OnGameEvent (wa);
				break;
			case ChestType.Closed_PirateChest_Empty:
				wa.ammont = 0;
				wa.treasureContains = TreasureHuntLib.TreasureContains.Empty;
				OnGameEvent (wa);
				break;
			}		
			try{
			var transaction = frag.BeginTransaction ();
			var dialogFragment = new WinNoticDialog (chestType, ammount, isText, text);
			dialogFragment.SetMenuVisibility (false);
			dialogFragment.SetStyle (DialogFragmentStyle.NoFrame, 0);
			dialogFragment.SetHasOptionsMenu (false);
			dialogFragment.Cancelable = false;
			dialogFragment.Show (transaction, "dialog_fragment");
			await Task.Delay (1500);
			dialogFragment.Dismiss ();
			}catch{
			}
		}


		private int MarginValue (int count)
		{
			return 120 / count;
		}

		private int pxFromDp (float dp, Context context)
		{
			return (int)(dp * context.Resources.DisplayMetrics.Density);
		}



	}
}

