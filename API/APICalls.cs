﻿using System;
using RestSharp;
using Newtonsoft.Json;
using TreasureGameModels;
using Android.Content;
using Android.Content.PM;
using System.Threading;
using System.Threading.Tasks;
using Android.App;


namespace TresureHunt
{
	public class APICalls
	{

        public static String baseUrl = "http://demoserver-th.azurewebsites.net/";


		public enum Method
		{
			GET,
			POST,
			PUT,
			DELETE}

		;

		static ApiResult result = null;

		// create new class that hold message for Create() and message out

		//------------------------------------------------------------------------------------------
		//Create                                   Call Treasure Hunt Web api
		//------------------------------------------------------------------------------------------
		//objectToUpdate : object                  Object to send, null for void
		//apiEndPoint    : string                  Url of the apicall e.g "api/AccountApi/JsonLogin"
		//postOrGet: APICalls.Method postOrGet     Post or Get
		//timeout : int                            if set, timeout for the request in ms
		//overrideBaseUrl : string                 if set, override the baseurl
		//-------------------------------------------------------------------------------------------
		public static async Task<ApiResponser> Create<T> (object objectToUpdate, string apiEndPoint, APICalls.Method postOrGet,
		                                                   int timeout = -1, string overrideBaseUrl = null, bool useCookie = true) where T : new()
		{
			String url2Use = baseUrl;
			RestSharp.Method _method = RestSharp.Method.GET;
			ApiResponser apiResponser = new ApiResponser ();
			switch (postOrGet) {
			case Method.GET:
				{
					_method = RestSharp.Method.GET;
					break;
				}
			case Method.POST:
				{
					_method = RestSharp.Method.POST;
					break;
				}
			case Method.PUT:
				{
					_method = RestSharp.Method.PUT;
					break;
				}
			case Method.DELETE:
				{
					_method = RestSharp.Method.DELETE;
					break;
				}
			}
			url2Use = (overrideBaseUrl == null) ? baseUrl : overrideBaseUrl;

			apiResponser.client = new RestClient (url2Use) {

			};
			apiResponser.client.AddDefaultHeader ("X-Requested-With", "XMLHttpRequest");

			var json = JsonConvert.SerializeObject (objectToUpdate);
			apiResponser.request = new RestRequest (apiEndPoint, _method);

			if (timeout > 0) {
				apiResponser.request.Timeout = 25000;
			} else {
				apiResponser.request.Timeout = 25000;
			}
			apiResponser.request.AddParameter ("text/json", json, ParameterType.RequestBody);

			return apiResponser;
		}


		public async Task<ApiResponser> restResponse(ApiResponser apiResponser){
			bool requestSucceeded = false;

			try {
				apiResponser.response = await apiResponser.client.ExecuteTaskAsync (apiResponser.request);
				requestSucceeded = (apiResponser.response != null) ? true : false;

				apiResponser.apiResult = new ApiResult () { Status = ApiResponse.OK ,};
			} catch (TimeoutException) {
				apiResponser.apiResult = new ApiResult () { Status = ApiResponse.NETWORK_ERROR };
				requestSucceeded = false;
			} catch {
				apiResponser.apiResult = new ApiResult () { Status = ApiResponse.OTHER_ERROR };
				requestSucceeded = false;
			}
			return apiResponser;
		}

		public async Task<ApiResponser> Signup_Real (LogInModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Signup/SignUp", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}

		public async Task<ApiResponser> Signup_Demo (LogInModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Signup/SignUpDemo", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}

		public async Task<ApiResponser> Login (LogInModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Login/Login", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}
        public async Task<ApiResponser> Authenticate(LogInModel model)
        {
            try
            {
                var x = await APICalls.Create<ApiResponser>(model, "api/Login/Authenticate", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }

		public async Task<ApiResponser> GetNewGame (LogInModel model)
		{

			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Session2/GetSession", APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}

		public static bool IsLoggedIn ()
		{
			if (result != null) {
				if (result.Status == ApiResponse.OK)
					return true;
				return false;
			}
			return false;
		}

        public async Task<ApiResponser> TransferToWallet(TransferModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser>(model, "api/Creel/TransferCurrencyToCreel", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}

        public async Task<ApiResponser> TransferToGame(TransferModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser>(model, "api/Creel/TransferCurrencyFromCreel", APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}
		public async Task<ApiResponser> BuyFromCreel(TransferModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser>(model, "api/Creel/BuyFromCreel", APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}
        public async Task<ApiResponser> RegisterCreel(LogInModel model)
        {
            try
            {
                var x = await APICalls.Create<ApiResponser>(model, "api/Creel/RegisterCreel", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }
		public async Task<ApiResponser> AuthorizationCheckCreel(LogInModel model)
		{
			try
			{
				var x = await APICalls.Create<ApiResponser>(model, "api/Creel/CheckAuthorizationState", APICalls.Method.POST, -1, null, true);
				return x;
			}
			catch
			{
				return null;
			}
		}
		public async Task<ApiResponser> UnRegisterCreel(LogInModel model)
		{
			try
			{
				var x = await APICalls.Create<ApiResponser>(model, "api/Creel/UnRegisterCreel", APICalls.Method.POST, -1, null, true);
				return x;
			}
			catch
			{
				return null;
			}
		}
        public async Task<ApiResponser> UserSearch(UserSearchCreel model)
        {
            try
            {
                var x = await APICalls.Create<ApiResponser>(model, "api/Creel/UserSearch", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }
		public async Task<ApiResponser> TransferToUser_Real (TransferToUserModel model)
		{
			try {
                var x = await APICalls.Create<ApiResponser>(model, "api/Creel/TransferToUser", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}
        public async Task<ApiResponser> GetTotalSpendingCapacity_Creel(TransferModel model)
        {
            try
            {
                var x = await APICalls.Create<ApiResponser>(model, "api/Creel/GetTotalSpendingCapacity_Creel", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }
        public async Task<ApiResponser> GetCurrentCapcity_Creel(TransferModel model)
        {
            try
            {
                var x = await APICalls.Create<ApiResponser>(model, "api/Creel/GetCurrentCapcity_Creel", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }
		public async Task<ApiResponser> GetStatus (LogInModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Buy/GetStatus", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}


		public async Task<ApiResponser> PeekPayment (LogInModel model)
		{
			try {

				var x = await APICalls.Create<ApiResponser> (model, "api/Buy/Peek", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}
		public async Task<ApiResponser> GetOverAllWalletBalnce (LogInModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Wallet/GetOverallBalance", APICalls.Method.POST, -1, null, true);
				return x;
			} catch (Exception e){
				return null;
			}
		}
		public async Task<ApiResponser> TradeExecute (AuthenticatedTradingModel authTradeModel)
		{
			try {

				var x = await APICalls.Create<ApiResponser> (authTradeModel, "api/Wallet/TradeCurrencies", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}
        public async Task<ApiResponser> getTransactionLogs(TransactionLogsModel transactionLogsModel)
        {
            try
            {

                var x = await APICalls.Create<ApiResponser>(transactionLogsModel, "api/Logs/GetTransactionLogs", APICalls.Method.POST, -1, null, true);
                return x;
            }
            catch
            {
                return null;
            }
        }
		public async Task<ApiResponser> Buy (WalletModel model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/Buy/Buy", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}

		public async Task<ApiResponser> SendEvent (GameEvent model)
		{
			try {
				var x = await APICalls.Create<ApiResponser> (model, "api/GameEvent/PostEvent", APICalls.Method.POST, -1, null, true);
				return x;
			} catch {
				return null;
			}
		}


		public async Task<ApiResponser> GetExchangeRate (string boughtCurrency, string offerCurrency )
		{
			try {
                var x = await APICalls.Create<ApiResponser>(null, "api/Wallet/GetExchangeRate?boughtCurrency=" + boughtCurrency + "&offerCurrency=" + offerCurrency, APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}

		public async Task<ApiResponser> RegisterForPush (string username, string ID, NotificationHubRegistration pushModel)
		{
			try {
				var x = await APICalls.Create<NotificationHubRegistration> (pushModel, "api/PushNotification/Register?platform=" + pushModel.Platform + "&userName=" + pushModel.UserName + "&deviceID=" + pushModel.DeviceId, APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}

		public async Task<ApiResponser> UnregisterForPush ( string username,NotificationHubRegistration pushModel)
		{
			try {
				var x = await APICalls.Create<NotificationHubRegistration> (pushModel, "api/PushNotification/UnregisterUser?userName=" + pushModel.UserName, APICalls.Method.POST, -1, null, true);
				return x;
			} catch{
				return null;
			}
		}

        public async Task<bool> Error_Send(Exception model)
        {
            try
            {
                var x = await APICalls.Create<Boolean>(model, "api/ErrorHandler/SendError", APICalls.Method.POST, -1, null, true);
                return true;
            }
            catch
            {
                return false;
            }
        }
	}
}

