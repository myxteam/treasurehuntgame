﻿using System;
using System.Threading.Tasks;
using TreasureGameModels;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using Android.OS;

namespace TresureHunt
{
    public class ApiCallsExtendtion
    {
        APICalls apiCalls;
        private static ApiCallsExtendtion instance;

        private ApiCallsExtendtion()
        {
            if (apiCalls == null)
                apiCalls = new APICalls();
        }

        public static ApiCallsExtendtion Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApiCallsExtendtion();
                }
                return instance;
            }
        }
        public async Task<ApiResult> apiDeserialize(ApiResponser apiResponser, BaseActivity context)
        {
            var restrepsonse = await restResponse(apiResponser, context);
            string content = restrepsonse.Content;

            ApiResult result = JsonConvert.DeserializeObject<ApiResult>(content);

            return result;
        }

        public void ErrorHandler(bool showMessage, Exception e)
        {
            if (showMessage)
            {
            }
        }

        public void MessageHandler_in(bool showMessage, BaseActivity context)
        {
            if (showMessage)
            {
                context.ShowLoadingDialog(context.BaseContext.GetText(Resource.String.contactingServer));
            }
        }

        public async Task<int> MessageHandler_Out(bool showMessage, ApiResult result, BaseActivity context)
        {
            bool retry = false;
            int outresult = 0;
            context.DismissLoadingDialog();
            if (showMessage)
            {
                if (result.Status == ApiResponse.NETWORK_ERROR)
                {
					retry = await context.ShowYesOrNoDialog(context.GetText(Resource.String.cannotContactServerRetry), context.GetText(Resource.String.yes),
						context.GetText(Resource.String.no));
                    if (retry)
                        outresult = 0;
                    else
                        outresult = 1;
                }
                else if (result.Status == ApiResponse.OTHER_ERROR)
                {
                    if (result.ErrorString == null)
                    {
						result.ErrorString = context.GetText(Resource.String.cannotContactServerRetry);
                    }
                    retry = await context.ShowOKDialog(result.ErrorString);
                    outresult = 2;
                }
                else if (result.Status == ApiResponse.CREEL_ERROR)
                {
                    outresult = 4;
                }
                else
                {
                    outresult = 3;
                }
                return outresult;
            }
            else
            {
                return outresult;
            }
        }

        public async Task<IRestResponse> restResponse(ApiResponser apiResponser, BaseActivity context)
        {
            var apiResponse = await apiCalls.restResponse(apiResponser);
            if (!apiResponse.requestSucces)
            {
                int res = await MessageHandler_Out(true, apiResponse.apiResult, context);
                if (res == 0)
                {
                    apiResponse = await apiCalls.restResponse(apiResponser);
                }
                else if (res == 1)
                {
                    context.Finish();
                    context.StartActivity(typeof(StarterActivity));
                }
                else if (res == 2)
                {
                    res = await MessageHandler_Out(true, apiResponse.apiResult, context);
                }
                return apiResponser.response;
            }
            else
            {
                return apiResponser.response;
            }
        }

        public async Task<ApiResult> Signup_Reals(LogInModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.Signup_Real(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
		public async Task<ApiResult> Signup_Demo(LogInModel model, bool showMessage, BaseActivity act)
		{
			try
			{
				MessageHandler_in(showMessage, act);
				var x = await apiCalls.Signup_Demo(model);
				var result = await apiDeserialize(x, act);
				return result;
			}
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
		}
        public async Task<ApiResult> Login(LogInModel model, bool showMessage, BaseActivity act)
        {
            ApiResult result = null;
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.Login(model);
                result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}

        }
        public async Task<ApiResult> Authenticate(LogInModel model, bool showMessage, BaseActivity act)
        {
            ApiResult result = null;
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.Authenticate(model);
                result = await apiDeserialize(x, act);
                int res = await MessageHandler_Out(showMessage, result, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}

        }
        public async Task<ApiResult> LoginCreel(LogInModel model, bool showMessage, BaseActivity act)
        {
            ApiResult result = null;
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.RegisterCreel(model);
                result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}
        }
		public async Task<ApiResult> AuthorizationCheckCreel(LogInModel model, bool showMessage, BaseActivity act)
		{
			ApiResult result = null;
			try
			{
				MessageHandler_in(showMessage, act);
				var x = await apiCalls.AuthorizationCheckCreel(model);
				result = await apiDeserialize(x, act);
				return result;
			}
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}
		}
		public async Task<ApiResult> UnRegisterCreel(LogInModel model, bool showMessage, BaseActivity act)
		{
			ApiResult result = null;
			try
			{
				MessageHandler_in(showMessage, act);
				var x = await apiCalls.UnRegisterCreel(model);
				result = await apiDeserialize(x, act);
				return result;
			}
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}

		}
        public async Task<ApiResult> UserSearch(UserSearchCreel model, bool showMessage, BaseActivity act)
        {
            ApiResult result = null;
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.UserSearch(model);
                result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}
        }
        public async Task<ApiResult> TransferToUser_Real(TransferToUserModel model, bool showMessage, BaseActivity act)
        {
            ApiResult result = null;
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.TransferToUser_Real(model);
                result = await apiDeserialize(x, act);
                 return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return result;
			}
        }
        public async Task<ApiResult> GetNewGame(LogInModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetNewGame(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> TransferToWallet(TransferModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);

                var x = await apiCalls.TransferToWallet(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> TransferToGame(TransferModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.TransferToGame(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
		public async Task<ApiResult> BuyFromCreel(TransferModel model, bool showMessage, BaseActivity act)
		{
			try
			{
				MessageHandler_in(showMessage, act);
				var x = await apiCalls.BuyFromCreel(model);
				var result = await apiDeserialize(x, act);
				return result;
			}
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
		}
        public async Task<ApiResult> GetStatus(LogInModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetStatus(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> PeekPayment(LogInModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.PeekPayment(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
            {
				apiCalls.Error_Send(ex);
                return null;
            }
        }

        public async Task<ApiResult> GetOverAllWalletBalnce(LogInModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetOverAllWalletBalnce(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
        public async Task<ApiResult> TradeExecute(AuthenticatedTradingModel authTradeModel, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.TradeExecute(authTradeModel);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
        public async Task<ApiResult> GetExchangeRate(string boughtCurrency, string offerCurrency, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetExchangeRate(boughtCurrency, offerCurrency);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> Buy(WalletModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.Buy(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> getTransactionLogs(TransactionLogsModel transactionLogsModel, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.getTransactionLogs(transactionLogsModel);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> SendEvent(GameEvent model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.SendEvent(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> RegisterForPush(bool showMessage, string username, string ID, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                NotificationHubRegistration pushModel = new NotificationHubRegistration();
                pushModel.DeviceId = ID;
                pushModel.UserName = username;
                pushModel.NotifyOnRegister = true;
                pushModel.Platform = "android";
                var x = await apiCalls.RegisterForPush(username, ID, pushModel);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }

        public async Task<ApiResult> UnregisterForPush(bool showMessage, string username, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                NotificationHubRegistration pushModel = new NotificationHubRegistration();
                pushModel.UserName = username;
                var x = await apiCalls.UnregisterForPush(username, pushModel);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }


        public async Task<ApiResult> GetCurrentCapcity_Creel(TransferModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetCurrentCapcity_Creel(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
        public async Task<ApiResult> GetTotalSpendingCapacity_Creel(TransferModel model, bool showMessage, BaseActivity act)
        {
            try
            {
                MessageHandler_in(showMessage, act);
                var x = await apiCalls.GetTotalSpendingCapacity_Creel(model);
                var result = await apiDeserialize(x, act);
                return result;
            }
			catch (Exception ex)
			{
				apiCalls.Error_Send(ex);
				return null;
			}
        }
		public async void Error_Send(Exception model)
		{
			try
			{
				 apiCalls.Error_Send(model);
			}
			catch(Exception ex)
			{
				apiCalls.Error_Send(model);
			}
		}
    }
}

